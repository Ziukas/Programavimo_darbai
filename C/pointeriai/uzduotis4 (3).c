#include <stdio.h>
#include <stdlib.h>
#include <math.h>
void splitData(int data[], int size, int split, int *pA, int *pB);

int main(){
    int Array[100], size, split, pA, pB;
    splitData(Array, size, split, &pA, &pB);
return 0;
}


void splitData(int data[], int size, int split, int *pA, int *pB){
    int i;
pA= malloc(split * sizeof(int));
if(!pA){
return -1;
}
pB = malloc(split * sizeof(int));
if (!pB) {
  return -1;
}
memcpy(pA, data, split * sizeof(int));
memcpy(pB, data + split, split * sizeof(int));
return pA, pB;
}




