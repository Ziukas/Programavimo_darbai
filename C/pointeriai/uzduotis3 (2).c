#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void swap(int *x, int *y);

int main(){
    int a=1, b=2;
    //printf("%d %d", a, b);       DEMO
    swap(&a,&b);
    //printf("%d %d", a, b);
    return 0;
}
void swap(int *x, int *y){
    int temp;
    temp=*x;
    *x=*y;
    *y=temp;
}
