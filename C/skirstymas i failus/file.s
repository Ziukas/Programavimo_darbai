	.file	"file.c"
	.section .rdata,"dr"
LC0:
	.ascii "wb\0"
	.text
	.globl	_saveToFile
	.def	_saveToFile;	.scl	2;	.type	32;	.endef
_saveToFile:
	pushl	%ebp
	movl	%esp, %ebp
	subl	$40, %esp
	movl	$LC0, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_fopen
	movl	%eax, -16(%ebp)
	movl	-16(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	$4, 8(%esp)
	movl	$1, 4(%esp)
	leal	16(%ebp), %eax
	movl	%eax, (%esp)
	call	_fwrite
	movl	$0, -12(%ebp)
	jmp	L2
L3:
	movl	-12(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	12(%ebp), %eax
	addl	%eax, %edx
	movl	-16(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	$1, 8(%esp)
	movl	$4, 4(%esp)
	movl	%edx, (%esp)
	call	_fwrite
	addl	$1, -12(%ebp)
L2:
	movl	16(%ebp), %eax
	cmpl	%eax, -12(%ebp)
	jl	L3
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_fclose
	movl	_save, %eax
	addl	$1, %eax
	movl	%eax, _save
	leave
	ret
	.section .rdata,"dr"
LC1:
	.ascii "rb\0"
	.text
	.globl	_loadFromFile
	.def	_loadFromFile;	.scl	2;	.type	32;	.endef
_loadFromFile:
	pushl	%ebp
	movl	%esp, %ebp
	subl	$40, %esp
	movl	$LC1, 4(%esp)
	movl	8(%ebp), %eax
	movl	%eax, (%esp)
	call	_fopen
	movl	%eax, -16(%ebp)
	movl	-16(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	$1, 8(%esp)
	movl	$4, 4(%esp)
	leal	16(%ebp), %eax
	movl	%eax, (%esp)
	call	_fread
	movl	$0, -12(%ebp)
	jmp	L5
L6:
	movl	-12(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	12(%ebp), %eax
	addl	%eax, %edx
	movl	-16(%ebp), %eax
	movl	%eax, 12(%esp)
	movl	$1, 8(%esp)
	movl	$4, 4(%esp)
	movl	%edx, (%esp)
	call	_fread
	addl	$1, -12(%ebp)
L5:
	movl	16(%ebp), %eax
	cmpl	%eax, -12(%ebp)
	jl	L6
	movl	-16(%ebp), %eax
	movl	%eax, (%esp)
	call	_fclose
	movl	_load, %eax
	addl	$1, %eax
	movl	%eax, _load
	leave
	ret
	.ident	"GCC: (tdm-1) 4.9.2"
	.def	_fopen;	.scl	2;	.type	32;	.endef
	.def	_fwrite;	.scl	2;	.type	32;	.endef
	.def	_fclose;	.scl	2;	.type	32;	.endef
	.def	_fread;	.scl	2;	.type	32;	.endef
