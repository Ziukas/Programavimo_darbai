#include <stdio.h>
#include "file.h"


void saveToFile(char* filename, int m[],int size){


    FILE *fd = fopen(filename, "wb");
    fwrite(&size , 1 , sizeof(size) , fd );
    int i;
    for (i = 0; i < size; i++)
            fwrite(&m[i], sizeof(m[i]), 1, fd);
    //save++;
    fclose(fd);

}

void loadFromFile(char* filename, int m[], int size){
    FILE *fr = fopen(filename, "rb");
    fread(&size, sizeof(size), 1, fr);
    int i;
    for(i=0; i<size; i++){
      fread(&m[i], sizeof(m[i]), 1, fr);
      //printf("%d", m[i]);

    }//load++;
    fclose(fr);

}
