	.file	"main.c"
	.globl	_save
	.bss
	.align 4
_save:
	.space 4
	.globl	_load
	.align 4
_load:
	.space 4
	.def	___main;	.scl	2;	.type	32;	.endef
	.section .rdata,"dr"
LC0:
	.ascii "file.bin\0"
LC1:
	.ascii "file2.bin\0"
LC2:
	.ascii "All good\0"
LC3:
	.ascii "something\222s wrong\0"
	.text
	.globl	_main
	.def	_main;	.scl	2;	.type	32;	.endef
_main:
	pushl	%ebp
	movl	%esp, %ebp
	andl	$-16, %esp
	subl	$144, %esp
	call	___main
	movl	$10, 140(%esp)
	movl	140(%esp), %eax
	movl	%eax, 4(%esp)
	leal	100(%esp), %eax
	movl	%eax, (%esp)
	call	_fillArray
	movl	140(%esp), %eax
	movl	%eax, 4(%esp)
	leal	20(%esp), %eax
	movl	%eax, (%esp)
	call	_fillArray
	movl	140(%esp), %eax
	movl	%eax, 4(%esp)
	leal	60(%esp), %eax
	movl	%eax, (%esp)
	call	_fillArray
	movl	140(%esp), %eax
	movl	%eax, 8(%esp)
	leal	100(%esp), %eax
	movl	%eax, 4(%esp)
	movl	$LC0, (%esp)
	call	_saveToFile
	movl	140(%esp), %eax
	movl	%eax, 8(%esp)
	leal	60(%esp), %eax
	movl	%eax, 4(%esp)
	movl	$LC0, (%esp)
	call	_saveToFile
	movl	140(%esp), %eax
	movl	%eax, 8(%esp)
	leal	100(%esp), %eax
	movl	%eax, 4(%esp)
	movl	$LC0, (%esp)
	call	_loadFromFile
	movl	140(%esp), %eax
	movl	%eax, 8(%esp)
	leal	20(%esp), %eax
	movl	%eax, 4(%esp)
	movl	$LC1, (%esp)
	call	_saveToFile
	movl	140(%esp), %eax
	movl	%eax, 8(%esp)
	leal	60(%esp), %eax
	movl	%eax, 4(%esp)
	movl	$LC1, (%esp)
	call	_loadFromFile
	movl	140(%esp), %eax
	movl	%eax, 8(%esp)
	leal	20(%esp), %eax
	movl	%eax, 4(%esp)
	movl	$LC0, (%esp)
	call	_loadFromFile
	movl	_save, %eax
	cmpl	$3, %eax
	jne	L2
	movl	_load, %eax
	cmpl	$3, %eax
	jne	L2
	movl	$LC2, (%esp)
	call	_printf
	jmp	L3
L2:
	movl	$LC3, (%esp)
	call	_printf
L3:
	movl	$0, %eax
	leave
	ret
	.globl	_fillArray
	.def	_fillArray;	.scl	2;	.type	32;	.endef
_fillArray:
	pushl	%ebp
	movl	%esp, %ebp
	pushl	%ebx
	subl	$36, %esp
	movl	$0, (%esp)
	call	_time
	movl	%eax, (%esp)
	call	_srand
	movl	$0, -12(%ebp)
	jmp	L6
L7:
	movl	-12(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	8(%ebp), %eax
	leal	(%edx,%eax), %ebx
	call	_rand
	movl	%eax, (%ebx)
	addl	$1, -12(%ebp)
L6:
	movl	-12(%ebp), %eax
	cmpl	12(%ebp), %eax
	jl	L7
	addl	$36, %esp
	popl	%ebx
	popl	%ebp
	ret
	.section .rdata,"dr"
LC4:
	.ascii "%d\12\0"
	.text
	.globl	_printArray
	.def	_printArray;	.scl	2;	.type	32;	.endef
_printArray:
	pushl	%ebp
	movl	%esp, %ebp
	subl	$40, %esp
	movl	$0, -12(%ebp)
	jmp	L9
L10:
	movl	-12(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	8(%ebp), %eax
	addl	%edx, %eax
	movl	(%eax), %eax
	movl	%eax, 4(%esp)
	movl	$LC4, (%esp)
	call	_printf
	addl	$1, -12(%ebp)
L9:
	movl	-12(%ebp), %eax
	cmpl	12(%ebp), %eax
	jl	L10
	movl	$10, (%esp)
	call	_putchar
	leave
	ret
	.section .rdata,"dr"
LC5:
	.ascii "something is wrong\0"
	.text
	.globl	_compare
	.def	_compare;	.scl	2;	.type	32;	.endef
_compare:
	pushl	%ebp
	movl	%esp, %ebp
	subl	$40, %esp
	movl	$0, -12(%ebp)
	movl	$0, -16(%ebp)
	jmp	L12
L14:
	movl	-16(%ebp), %eax
	leal	0(,%eax,4), %edx
	movl	8(%ebp), %eax
	addl	%edx, %eax
	movl	(%eax), %edx
	movl	-16(%ebp), %eax
	leal	0(,%eax,4), %ecx
	movl	12(%ebp), %eax
	addl	%ecx, %eax
	movl	(%eax), %eax
	cmpl	%eax, %edx
	jne	L13
	addl	$1, -12(%ebp)
L13:
	addl	$1, -16(%ebp)
L12:
	movl	-16(%ebp), %eax
	cmpl	16(%ebp), %eax
	jl	L14
	movl	-12(%ebp), %eax
	cmpl	16(%ebp), %eax
	jne	L15
	movl	$LC2, (%esp)
	call	_printf
	jmp	L11
L15:
	movl	$LC5, (%esp)
	call	_printf
L11:
	leave
	ret
	.ident	"GCC: (tdm-1) 4.9.2"
	.def	_saveToFile;	.scl	2;	.type	32;	.endef
	.def	_loadFromFile;	.scl	2;	.type	32;	.endef
	.def	_printf;	.scl	2;	.type	32;	.endef
	.def	_time;	.scl	2;	.type	32;	.endef
	.def	_srand;	.scl	2;	.type	32;	.endef
	.def	_rand;	.scl	2;	.type	32;	.endef
	.def	_putchar;	.scl	2;	.type	32;	.endef
