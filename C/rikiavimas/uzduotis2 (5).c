#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void fillArray(int m[], int size);
void printArray(int m[], int size);
void A(int m[], int size);            //2a
void B(int m[], int size);            //2b
void E(int m[], int size);            //2f
void F(int m[], int size);            //2f
int check(int m[], int size);

enum BOOLEAN {FALSE, TRUE};

int main(){
    int Array[10], size=10;
    fillArray(Array,size);
    printArray(Array,size);
    check(Array,size);
    F(Array,size);
    printArray(Array,size);
    check(Array,size);
return 0;
}


void fillArray(int m[], int size){
int i;
srand(time(NULL));
for (i=0; i<size; i++){
    m[i]=rand()%30;
    }

}

void printArray(int m[], int size){
    int i;
for (i=0; i<size; i++)
    printf("%d\n", m[i]);
    printf("\n");
}

void A(int m[], int size){
     int i, j;
    for(i=0; i<size; i++){
		for(j=0; j<size-1; j++){
			if(m[j]>m[j+1]){
				int temp = m[j+1];
				m[j+1] = m[j];
				m[j] = temp;}
        }
	}
}

void B( int m[], int n){
    int i;
    int j;
    for (i = 0; i < n; i++){
        int min = i;
        for (j = i; j < n; j++){
            if( m[min] > m[j] ){
                min = j;
            }
        }
        int tarp = m[i];
        m[i] = m[min];
        m[min] = tarp;
    }
}
void E ( int m[], int size){
    int i, j;
    int tarp;
    for (i = 0 ; i < size; i++){
        for (j = 0 ; j < size-1; j++ ){
            if (m[j] > m[j+1]){
                tarp = m[j];
                m[j] = m[j+1];
                m[j+1] = tarp;
                if (m[j+1] > m[j+2] && m[j+1] < m[j+3] && j+1 != size && j+2 != size && j+3 != size){
                    tarp = m[j+1];
                    m[j+1] = m[j+2];
                    m[j+2] = tarp;
                }
            }
        }
    }

}

void F(int m[], int size){
int j;
int temp;
int limit = size;
int s = -1;
enum BOOLEAN swapped;
limit--;
while(s < limit){
    swapped = FALSE;
    s++;
    limit--;
    for(j=s; j<limit; j++){
        if(m[j] > m[j+1]){
            temp = m[j];
            m[j] = m[j+1];
            m[j+1] = temp;
            swapped = TRUE;
            }
        }
    if(!swapped) return;
    swapped = FALSE;
    for(j=limit; j>=s; --j){
        if (m[j]>m[j + 1]){
            temp = m[j];
            m[j] = m[j+1];
            m[j+1] = temp;
            swapped = TRUE;
            }
       }
    if(!swapped)return;
    }
return;
}

int check(int m[], int size){
int i;
int c=0;
for (i=0; i<size-1; i++ ){
    if (m[i]<=m[i+1]){
            c=1;
    }
    else{
        c=0;
        break;
    }
}
if (c==1) printf ("Correct\n");
else printf("Not correct\n");
}
