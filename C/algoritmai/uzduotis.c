#include <stdio.h>
#include <stdlib.h>
#include <time.h>

float seconds;
int timec;
double Comp[3];
long int Att[3];
float T[3];
void fillArray(int m[], int size);
int check(int m[], int size);
void InsertionSort ( int m[], int size);
void selection (int m[], int size);
void merge(int Array[], int l, int m, int r);
void mergeSort(int Array[], int l, int r);
void times(int Array[], int size);
void printArray(int m[], int size);

//selection(Array,size, At[]);

int main(){
    int Array[40000], size=40000;
    fillArray(Array,size);
    InsertionSort (Array, size);
    selection(Array,size);
    mergeSort(Array, 0, size-1);
    printf("*********************************************************\n");
    printf("               %16s %16s %16s", "Attachment", "Comparison", "Time(s)\n");
    printf("InsertionSort: %16d %16.0f %16f\n", Att[0], Comp[0], T[0] );
    printf("SelectionSort: %16d %16.0f %16f\n", Att[1], Comp[1], T[1]);
    printf("MergeSort:     %16d %16.0f %16f\n", Att[2], Comp[2], T[2]);
    printf("*********************************************************\n");

return 0;
}


void fillArray(int m[], int size){
    int i;
    srand(time(NULL));
    for (i=0; i<size; i++){
        m[i]=rand()%30;
    }

}
void printArray(int m[], int size){
    int i;
    for (i=0; i<size; i++)
        printf("%d\n", m[i]);
        printf("\n");
}
int check(int m[], int size){
int i;
int c=0;
for (i=0; i<size-1; i++ ){
    if (m[i]<=m[i+1]){
            c=1;
    }
    else{
        c=0;
        break;
    }
}
    if (c==1) printf ("Correct\n");
    else printf("Not correct\n");
}


void InsertionSort ( int m[], int size){
      clock_t start = clock();

    int j, i, t;
    Att[0]=0;
    Comp[0]=0;
    Comp[0]=Comp[1]+size-1;
    for (i = 1 ; i <= size - 1; i++) {
        j = i; Att[0]=Att[0]+1;

        while ( j > 0 && m[j] < m[j-1]) {
            Comp[0]=Comp[0]+2;
            t = m[j];
            m[j]= m[j-1];
            m[j-1] = t;
            j--;

    }

    Att[0]=Att[0]+3;
  }
    clock_t end = clock();
    T[0] = (float)(end - start) / CLOCKS_PER_SEC;

}
void selection (int m[], int size){
    clock_t start = clock();
    int swap;
    int j;
    Att[1]=0;
    Comp[1]=Comp[1]+size-1;
    for (j = 0; j < size-1; j++)
    {
        int min = j; Att[1]=Att[1]+1;
        int i;

        for ( i = j+1; i < size; i++) {
            if (m[i] < m[min])
            {
                min = i; Att[1]=Att[1]+1;
            }

        }
        Comp[1]=Comp[1]+size*2;
        if(min != j)
        {
            swap = m[min];
            m[min] = m[j];
            m[j] = swap;
            Att[1]=Att[1]+3;
        }

    }
    clock_t end = clock();
    T[1] = (float)(end - start) / CLOCKS_PER_SEC;
}

void merge(int Array[], int l, int m, int r)
{
    int cnt=0;
    Att[2]=0;
    Comp[2]=0;
    int i, j, k;
    int n1 = m - l + 1;
    int n2 =  r - m;
    int L[n1], R[n2];

    for (i = 0; i < n1; i++){
        L[i] = Array[l + i]; Att[2]=Att[2]+1;} Comp[2]=Comp[2]+n1;

    for (j = 0; j < n2; j++){
        R[j] = Array[m + 1+ j]; Att[2]=Att[2]+1;} Comp[2]=Comp[2]+n2;
    i = 0; Att[2]=Att[2]+1;
    j = 0; Att[2]=Att[2]+1;
    k = l; Att[2]=Att[2]+1;
    while (i < n1 && j < n2)
    {
        Comp[2]=Comp[2]+2;
        if (L[i] <= R[j])
        {

            Array[k] = L[i]; Att[2]=Att[2]+1;
            i++;
        }

        else
        {
            Array[k] = R[j]; Att[2]=Att[2]+1;
            j++;
        }
        k++;
        cnt++;
    }
    Comp[2]=Comp[2]+cnt;
    while (i < n1)
    {
        Comp[2]=Comp[2]+1;
        Array[k] = L[i]; Att[2]=Att[2]+1;
        i++;
        k++;
    }

    while (j < n2)
    {
        Comp[2]=Comp[2]+1;
        Array[k] = R[j]; Att[2]=Att[2]+1;
        j++;
        k++;
    }

}
void mergeSort(int Array[], int l, int r)
{
    clock_t start = clock();

    if (l < r)
    {
        int m = l+(r-l)/2;
        mergeSort(Array, l, m);
        mergeSort(Array, m+1, r);
        merge(Array, l, m, r);

    }
    clock_t end = clock();
    T[2] = (float)(end - start) / CLOCKS_PER_SEC;
}


