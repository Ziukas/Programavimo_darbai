#include <stdio.h>
#include <stdlib.h>
#include <math.h>


int main(){
    int a, b, c, tarp, id;
    int val;
    int x[10]={0};                      // a)nuliniu reiksmiu priskyrimas
    for (int i = 0; i<10; i++)            // b) viso masyvo spausdinimas
        printf("%d \n", x[i]);
    printf("\n");
    x[0]=1;                             // c) reiksmiu priskyrimas
    x[3]=2;
    x[9]=3;
    for (int i = 2; i<10-1; i++)    // d) elemento trynimas
        x[i] = x[i+1];
    printf("\n");
    for (int i = 10-1; i>=7-1; i--)    // e) elemento iterpimas
        x[i+1]=x[i];
    x[7-1]=4;
    printf("\n");
    for (int i=0; i<=10-1; i++)        // f) spausdina masyva
        printf("%d \n", x[i]);
    printf("\n");
    printf("Enter 2 numbers \n");  // g) pakeicia ivesto masyvo reiksme i b
    scanf("%d %d", &a, &b);
    printf("\n");
    x[a-1]=b;
    printf ("Enter index of array, you want to delete \n"); // h) istrina pasirinkta elementa
    scanf("%d", &c);
    printf("\n");
    for (int i = c-1; i<10-1; i++)
        x[i]=x[i+1];
    printf("Enter two numbers to insert element into array  \n");                     // i) Iterpia nauja reiksme i pasirinkta vieta
    scanf("%d %d", &id, &val);
    printf("\n");
    for (int i = 10-1; i>=id-1; i--)
        x[i+1]=x[i];
    x[id-1]=val;
    for (int i = 0; i<10; i++)                     // j) spausdina galutini masyva
        printf("%d \n", x[i]);


return 0;
}
