//Uzduoti atliko Karolis Ziukas
//2016-10-22

#include <stdio.h>
#include <stdlib.h>
#include <math.h>


int main(){
    int a, i;
    int fact = 1;
    FILE *fp;
    printf("Enter positive number, that you want to get factorial number\n");
    scanf("%d", &a);
    printf("Data scaned\n");
    if (a<0)
        printf("You entered wrong number, restart program and try again");
    else
    {
        fp = fopen("out.txt", "w");           //Skaiciuoja faktoriala
        for (i = a; i>=1; i--)
        {
            fact=fact * i;

        }
        fprintf(fp, "Factorial number of %d is %d", a, fact);
        printf("Factorial number of %d is %d\n", a, fact);
        printf("Also, result is printed in file out.txt");
    }
    return 0;
}
