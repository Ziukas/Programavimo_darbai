//Uzduoti atliko Karolis Ziukas
//2016-10-23

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

//PATIKRINTI
int main(){
    char day[8][20];
    int i;
    FILE *fp;
    int y, m, d, ly, lm, ld, weekday;
    printf("Enter date in format like this: YYYY-MM-DD \n");
    printf("This program will calculate, which day of the week it is \n");
    printf("IMPORTANT! Make sure you enter date with a dashes, otherwise program won't work \n");
    scanf("%d-%d-%d" ,&y, &m, &d);
    printf("Data scaned\n");
    ly=floor(log10(abs(y))) + 1;     //Patikrina, is kiek skaiciu tada sudaryta
    lm=floor(log10(abs(m))) + 1;
    ld=floor(log10(abs(d))) + 1;
    if (m<=0 || m>12 || d<=0 || d>31)
        printf("There is no such date, restart program and try again");
    else if (ly==4 && (lm==2 || lm==1) && (ld==2 || ld==1))//skaiciuoja kuri savaites diena ivesta, sekmadienis = 0
        weekday  = (d+=m<3?y--:y-2,23*m/9+d+4+y/4-y/100+y/400)%7;    //Algoritmas pasiskolintas is interneto
    else printf
        ("Date was entered incorrectly, restart program and try again");
    fp=fopen("days.txt", "r");  //Atidaro faila dienu skaitymui
    i=1;
    while (fgets(day[i],20,fp)!=NULL) //Nuskaito dienas is failo
        i++;
    if (weekday!=0)
        printf ("Day of the week is: %s", day[weekday]); //Gauta savaites dienos reiksme ikelia i masyva
    else
        printf ("Day of the week is: %s", day[7]);

return 0;
}
