#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "header.h"

// TO DO: sutavrkyti filtrus;


int Menu();
void toTxt();
int erase(void);
void edit();
int eraseAll();
void search();
void filter1();
void filter2();
void filter3();
void filter4();
void sort(Cars *cars);
int check(Cars *cars);


void times(){
clock_t start = clock();
    Cars cars[50];
    Menu(cars);

clock_t end = clock();
float seconds = (float)(end - start) / CLOCKS_PER_SEC;
printf("program was running for %f seconds\n", seconds);}



int main()
{
    atexit(times);
    return 0;
}

int Menu(Cars *cars){
    int ch;
    printf("===================================================================\n");
    printf("*************************DATA MANAGEMENT MENU**********************\n");
    printf("===================================================================\n");
    printf("Please select from the option below:\n");
    printf("\t [1] Enter information about car\n");
    printf("\t [2] Store information in cars.txt file\n");
    printf("\t [3] See all information\n");
    printf("\t [4] Delete car information\n");
    printf("\t [5] Edit cars information\n");
    printf("\t [6] Erase all entered information\n");
    printf("\t [8] To see filtering options");
    printf("\t [0] Exit program\n");
    printf("***Enter your choice:***\n\t");

    while(1){
        scanf("%d", &ch);
        if (ch==0) break;
        switch(ch){
        case 1:{enter(&cars); break;}
        case 2:{toTxt(); break;}
        case 3:{display(&cars); break;}
        case 4:{erase(); break;}
        case 5:{edit(); break;}
        case 6:{eraseAll(); break;}
        //case 7:{sort(cars); break;}
        case 8:{filter(); break;}
        //case 9:
    default:

        printf("!!!Whoops! Choice you have entered does not exist. Please try again!!!\n");
        printf("\n");
        break;
    };
    printf("Please select from the option below:\n");
    printf("\t [1] Enter information about car\n");
    printf("\t [2] Store information in cars.txt file\n");
    printf("\t [3] See all information\n");
    printf("\t [4] Delete car information\n");
    printf("\t [5] Edit cars information\n");
    printf("\t [6] Erase all entered information\n");
    printf("\t [0] Exit program\n");
    printf("\t [8] To see filtering options");
    printf("***Enter your choice:***\n\t");}
}

int filter(Cars *cars){
    int ch;
    printf("Enter how you want to filter:\n");
    printf("\t [1] Find cars with exact name\n");
    printf("\t [2] Find cars by model\n");
    printf("\t [3] Find cars in years range\n");
    printf("\t [4] Find cars that cost less than you entered\n");
    printf("\t [0] Go back to the main menu\n");
    printf("***Enter your choice:***\n\t");
    while(1){
        scanf("%d", &ch);
        if (ch==0) break;
        switch(ch){
        case 1:{filter1(); break;}
        case 2:{filter2(); break;}
        case 3:{filter3(); break;}
        case 4:{filter4(); break;}
        case 0:{Menu(cars); break;}
    default:

        printf("!!!Whoops! Choice you have entered does not exist. Please try again!!!\n");
        printf("\n");
        break;
    };


    printf("Enter how you want to filter:\n");
    printf("\t [1] Find cars with exact name\n");
    printf("\t [2] Find cars by model\n");
    printf("\t [3] Find cars in years range\n");
    printf("\t [4] Find cars that cost less than you entered\n");
    printf("\t [0] Go back to the main menu\n");
    printf("***Enter your choice:***\n\t");}
}


    void toTxt(){
    FILE *fpbin, *fptxt;
    fptxt=fopen("cars.txt", "w");
    fpbin = fopen("carsdata.bin", "rb");
    if(fpbin==NULL) {printf("Binary file is empty, please enter some information first\n\n");}
    else {while (fread(&cars, sizeof(cars), 1, fpbin) != 0)
    {
        fprintf(fptxt,"\t%16s", cars.name);
        fprintf(fptxt,"\t%16s", cars.model);
        fprintf(fptxt,"\t%16d", cars.date);
        fprintf(fptxt,"\t%16d", cars.price);
        fprintf(fptxt,"\n");
    }
    printf("\nInformation saved to cars.txt file!\n\n");
    fclose(fpbin);
    fclose(fptxt);
 }}



int erase(void) {
	FILE *fp;
	FILE *fptmp;
	int found=0;
    char name[50];
	fp=fopen("carsdata.bin", "rb");
	if (fp==NULL) {
		printf("Unable to open file\n");
		return -1;
	}
	fptmp=fopen("tmp.bin", "wb");
	if (fptmp==NULL) {
		printf("Unable to open file temp file.\n");
		return -1;
	}
	printf("\n Enter car name you want to delete:\n");
	scanf("%s", &name);
	while (fread(&cars,sizeof(cars),1,fp) != NULL) {
		if (strcmp (name, cars.name) == 0) {
			printf("A record with requested name found and deleted.\n\n");
			found=1;
		} else {
			fwrite(&cars, sizeof(cars), 1, fptmp);
		}
	}
	if (found==0) {
		printf("No record(s) found with the requested name: %s\n\n", name);
	}
	fclose(fp);
	fclose(fptmp);
	remove("carsdata.bin");
	rename("tmp.bin", "carsdata.bin");
	return 0;
}

void edit(void)
{
    char name[50];
    FILE *fp;
    FILE *fptmp;
    label:
        fp=fopen("carsdata.bin","rb");
        fptmp=fopen("tmp.bin","wb");
        if (fp==NULL) printf ("\nFile that you want to edit is empty.\nPlease enter some information first\n\n");
        else{
        printf("Enter the name of a car you want to edit:\n");
        scanf("%s",name);
        while(fread(&cars,sizeof(cars),1,fp)==1)
        {

        if(strcmp(name,cars.name)!=0){
                fwrite(&cars,sizeof(cars),1,fptmp);};
        }

        printf("Enter car manufacturer name:\n");
        scanf("%s",cars.name);
        printf("Enter car brand name:\n");
        scanf("%s",cars.model);
        label1:
        printf("Enter car make year:\n");
        scanf("%d",&cars.date);
            if (cars.date<1900 || cars.date>2016 || cars.date<0){printf("\nEnter correct date\n"); goto label1;}
        label2:
        printf("Enter car price:\n");
        scanf("%d",&cars.price);
            if(cars.price<0){printf("\nEnter correct price\n"); goto label2;}
        printf("\n");
        fwrite(&cars,sizeof(cars),1,fptmp);
        fclose(fp);
        fclose(fptmp);
        remove("carsdata.bin");
        rename("tmp.bin","carsdata.bin");

}}

int eraseAll(){
     int i;
     FILE *fp;
     FILE *fr;
     fp=fopen("carsdata.bin", "rb");
     fr=fopen("cars.txt", "r");
    if ((fp==NULL) && (fr==NULL)){ printf("\nThere is nothing to erase\n\n"); return -1; }
    printf("\nAre you sure that you want to erase all information?");
    printf("\n\t[1] - Yes \t [0] - No\n");
    scanf("%d", &i);
    if (i==1){
    fclose(fp);
    fclose(fr);
    remove("carsdata.bin");
    remove("cars.txt");
    printf("\nAll information erased!\n\n");}
}
void filter3(){                   //filter by date
    FILE *fp;
fp=fopen("carsdata.bin", "rb");
int date, date1;
printf("\n Enter car year interval");
	scanf("%d\n %d", &date, &date1);
	while (fread(&cars,sizeof(cars),1,fp) != NULL)
    if (cars.date>date && cars.date<date1){
        printf("\t%16s", cars.name);
        printf("\t%16s", cars.name);
        printf("\t%16s", cars.model);
        printf("\t%16d", cars.date);
        printf("\t%16d", cars.price);
        printf("\n");

    }
    fclose(fp);
}

void filter1(){           //filter by name
    char name[50];
    FILE *fp;
fp=fopen("carsdata.bin", "rb");
printf("\n Enter car manufacturer that you want to see");
scanf("%s", name);
while (fread(&cars,sizeof(cars),1,fp) != NULL)
 if (strcmp (name, cars.name) == 0){
        printf("\t%16s", cars.name);
        printf("\t%16s", cars.model);
        printf("\t%16d", cars.date);
        printf("\t%16d", cars.price);
        printf("\n");

    }
    fclose(fp);
}
void filter4(){           //filter by price
    int price;
    FILE *fp;
fp=fopen("carsdata.bin", "rb");
printf("\n Enter max price point");
scanf("%d", &price);
while (fread(&cars,sizeof(cars),1,fp) != NULL)
 if (cars.price<=price){
        printf("\t%16s", cars.name);
        printf("\t%16s", cars.model);
        printf("\t%16d", cars.date);
        printf("\t%16d", cars.price);
        printf("\n");

    }
    fclose(fp);
}


void filter2(){           //filter by model
    char model;
    FILE *fp;
fp=fopen("carsdata.bin", "rb");
printf("\n Enter car model that you want to find");
scanf("%s", model);
while (fread(&cars,sizeof(cars),1,fp) != NULL)
  if(strcmp (model, cars.name) == 0)
   {
        printf("\t%16s", cars.name);
        printf("\t%16s", cars.model);
        printf("\t%16d", cars.date);
        printf("\t%16d", cars.price);
        printf("\n");

    }
    fclose(fp);
}
int check(Cars cars[]){
        FILE *fp;
        int k=0;
        fp = fopen("carsdata.bin", "rb");
  while(!feof(fp)){
        fread(&cars[k].name, sizeof(cars[k].name), 1, fp);
        fread(&cars[k].model, sizeof(cars[k].model), 1, fp);
        fread(&cars[k].date, sizeof(cars[k].date), 1, fp);
        fread(&cars[k].price, sizeof(cars[k].price), 1, fp);
        k++;
       }
       k--;
       fclose(fp);
       return k;
}
/*void sort(Cars *cars){

{
    FILE *fPointer;
    fPointer = fopen("carscist.bin", "rb+");
    int k;
    for(k=0; k < check(cars); k++)
    {
        fread(&cars[k].name, sizeof(cars[k].name), 1, fPointer);
        fread(&cars[k].model, sizeof(cars[k].model), 1, fPointer);
        fread(&cars[k].date, sizeof(cars[k].date), 1, fPointer);
        fread(&cars[k].price, sizeof(cars[k].price), 1, fPointer);
//        printf("\n%d)%s - %s - %d year - %d $\n", k+1, car[k].carName, car[k].carModel, car[k].year, car[k].price);
    }
    fclose(fPointer);
    int b=0;
    Cars temp;
    int p;
    for(p=0; p < check(cars); p++)
    {
        int z;
        for(z=0; z < check(cars)-1; z++)
        {

            if(cars[z].date > cars[z+1].date)
            {
                temp=cars[z];
                cars[z]=cars[z+1];
                cars[z+1]=temp;
            }
        }
    }
    int m;
    for(m=0; m < check(cars); m++)
    {
        printf("\t%16s", cars[m].name);
        printf("\t%16s", cars[m].model);
        printf("\t%16d", cars[m].date);
        printf("\t%16d", cars[m].price);
        printf("\n");
    }
}
}*/



