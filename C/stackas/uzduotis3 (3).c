#include <stdio.h>
#include <stdlib.h>
#include <math.h>
typedef struct{
    double x, y;
}Point;

typedef struct {
    Point *Array;
    size_t size;
}Stack;


void initStack(Stack *stack);
void printStack(Stack *stack);
int getStackSize(Stack *stack);
Point push(Stack *stack,  Point value);
Point top(Stack *stack);
Point pop(Stack *stack);
void destroyStack(Stack *stack);
Point createPoint(double x, double y);
int main ()
{
    Stack stack;
    Point point;
    initStack(&stack);

    push(&stack,createPoint(4,3));
    push(&stack,createPoint(5,3));
    push(&stack,createPoint(1,1));
    push(&stack,createPoint(7,6));
    push(&stack,createPoint(3,3));
    printStack(&stack);
return 0;
}
Point createPoint(double x, double y){
    Point p;
    p.x=x;
    p.y=y;
    return p;
}

void initStack(Stack *stack){
stack->Array = NULL;
stack->size=0;

}

void printStack(Stack *stack){
    double distance;
    int i;
    for (i=0; i<stack->size; i++){

        printf("coordinate of a point is (%.1f; %.1f)\n", stack->Array[i].x, stack->Array[i].y);
    distance=sqrt(pow(stack->Array[i].x, 2)+pow(stack->Array[i].y, 2));
    printf("Distence to the point (0;0) is: %.1f\n", distance);}

}

int getStackSize(Stack *stack){

return stack->size;

}

Point push(Stack *stack, Point value)
{

    if(stack->Array != NULL){

        (stack->Array) = realloc(stack->Array, (stack->size+1) * sizeof(Point));
        stack->Array[stack->size]=value;
        stack->size++;
    }
    else{
        stack->Array = malloc(stack->size+1 * sizeof(Point));
        stack->Array[stack->size]=value;
        stack->size++;
    }
}


Point top(Stack *stack){
    if(stack->Array==NULL)
    {
        Point a;
        a.x=0;
        a.y=0;
        return a;
    }

    else return stack->Array[stack->size-1];

}

Point pop(Stack *stack){

    if (stack->size==0)  return top(&stack);
    else {
        stack->size--;
        stack->Array=realloc(stack->Array, stack->size * sizeof(int));
        return top(stack);
    }


}
void destroyStack(Stack *stack){
    free(stack->Array);
    stack->Array=NULL;
    stack->size=0;
}
