#include <stdio.h>
#include <stdlib.h>


typedef struct {
    int *Array;
    size_t size;
}Stack;


void initStack(Stack *stack);
void printStack(Stack *stack);
int getStackSize(Stack *stack);
void push(Stack *stack,  int   value);
int top(Stack *stack);
int pop(Stack *stack);
void destroyStack(Stack *stack);
int main ()
{
return 0;
}

void initStack(Stack *stack){
stack->Array = NULL;
stack->size=0;

}

void printStack(Stack *stack){
    int i;
    for (i=0; i<stack->size; i++){

        printf("%d \n", stack->Array[i]);}

}

int getStackSize(Stack *stack){
return stack->size;


}

void push(Stack *stack, int value)
{


    if(stack->Array != NULL){
        stack->Array = realloc(stack->Array, (stack->size+1) * sizeof(int));
        *(stack->Array+stack->size)=value;
        stack->size++;
    }
    else{

        stack->Array =malloc(stack->size+1 * sizeof(int));
        *(stack->Array+stack->size)=value;
        stack->size++;
    }
}


int top(Stack *stack){
    if(stack->Array==NULL)return 0;

    else return stack->Array[stack->size-1];

}

int pop(Stack *stack){

    if (stack->size==0) return 0;
    else {
        stack->size--;

        stack->Array= realloc(stack->Array, stack->size * sizeof(int));
        top(&stack);
    }


}
void destroyStack(Stack *stack){
    free(stack->Array);
    stack->Array=NULL;
    stack->size=0;
}


