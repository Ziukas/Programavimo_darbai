#include <stdio.h>
#include <stdlib.h>
#include <math.h>

typedef struct{
    double x, y;
}Point;

void printPoint(Point p);
Point createPoint(double x, double y);
double getDistance(Point a, Point b);

int main ()
{

    getDistance(createPoint(5,7), createPoint(3, 2));

return 0;
}
void printPoint(Point p){
    printf("(%.1f %.1f)", p);
}

Point createPoint(double x, double y){
    Point p;
    p.x=x;
    p.y=y;
    return p;
}
double getDistance(Point a, Point b){
    double range;
    range=sqrt(pow(b.x-a.x, 2)+pow(b.y-a.y, 2));
    printf("%.1f", range); //DEMO
    return range;
}
