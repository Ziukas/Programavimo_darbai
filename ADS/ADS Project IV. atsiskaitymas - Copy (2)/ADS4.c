#include <stdio.h>
#include <stdlib.h>

int y = 0;

typedef int bool;
enum { false, true };

typedef struct Node
{
    int data;
    struct node *next;
} node;

typedef struct Que
{
    struct Node *front, *rear;
} que;



int push(que *q,int num);        //Ideda elementa i eiles gala
int deque(que *q);
int print(que *q);
que *init();


int main()
{
    que *q=init();
    FILE *fp;
    fp = fopen("graph.txt", "r");
    //Nuskanuoja matricos dydi
    //int N = 20;
    int N;
    fscanf (fp, "%d", &N);
    int kiek=1;
    int pirmas = 0;
    int graph[N+1][N+1];
    bool pazymeti[N+1][N+1];
    int V[N+1];
    int Tree[N+1];
    int i, j;
    for (i = 1; i<=N; i++)
    {
        V[i]=0;
        for (j=1; j<=N; j++)
        {
            graph[i][j]=0;
            pazymeti[i][j] = false;
        }
    }
    while(!feof(fp))
    {
        int a, b;
        fscanf (fp, "%d %d", &a, &b);
        graph[a][b]=1;
        graph[b][a]=1;
    }

    for (i = 1; i<=N; i++)
    {
        for (j=1; j<=N; j++)
        {
            printf("%d ", graph[i][j]);
        }
        printf("\n");
    }

    //Suranda visas virsunes

    for (i = 1; i<=N; i++)
    {

        for (j=1; j<=N; j++)
        {
            if(graph[i][j]==1)
            {
                V[i]=i;
                printf("%d ", V[i]);
                //printf(" !-%d-!\n ", j);
                break;
            }
        }
        printf("\n");
    }
//
//    Suranda, kuri pirma virsune
//    Ir pushina ja i eile
//    bei i medzio masyva ideda
    int k;
    for (i = 1; i<=N; i++)
    {

        if (pirmas==0)
        {
            for (j = 1; j<=N; j++)
            {
                if(V[j]!=0)
                {
                    pirmas = 1;
                    printf("%d \n", V[j]);  //j
                    pazymeti[i][j]== true;
                    pazymeti[j][i]== true;
                    push(q, V[j]);  //j
                    Tree[0]=V[j];  //j
                    break;
                }
            }
        }

        for (k = j; k<=N; k++)
        {
            //printf("%d " , graph[i][k]);
            if ((graph[i][k]!=0) && (pazymeti[i][k]!= true))
            {

                push(q, k);
                Tree[kiek]=k;
                pazymeti[i][k]=true;
                pazymeti[k][i]=true;
                kiek++;
                int x, y;
                for (x = 1; x<=N; x++)
                {
                    for (y=1; y<=N; y++)
                    {
                        if(graph[y][k]==0)
                        {
                            pazymeti[x][k]=true;
                        }
                    }
                }
            }
        }
        //printf("\n");
        deque(q);
    }

    for (i=0; i<kiek; i++)
    {
        printf("%d ", Tree[i]);

    }
    return 0;
}


int deque(que *q)
{
    node *temp;
    if (q->front == NULL || y==0)
    {
        printf("Eile tuscia, nera ka trinti\n");
        return 0;
    }

        else if(q->front == q->rear){
        q->front = q->rear = NULL;
        return 0;
    }
    else
    {
        temp = q->front;                        //laikina elementa prilygina priekiui
        q->front=q->front->next;                //prieki prilygina sekanciam elementui
        printf("Elementas: %d istrintas\n", *temp); //atspausdina elementa kuris buvo istrintas
        free(temp);                                 //atlaisvina atminti, kuria turejo laikinas elemntas
        return 0;
    }
}


int print(que *q)
{
    node *temp = q->front;

    if (q->front==NULL && q->rear==NULL)
    {
        printf("Eile tuscia, nera ka spausdinti\n");
        return 0;
    }

    printf("Eiles elementai: \n");
    while(temp!=NULL)
    {
        printf("%d\n", temp->data);
        temp = temp->next;
    }
    return 0;
}

que *init()
{
    y=1;
    que *q = (que*)malloc(sizeof(que));   //skiria atminti
    q->front=NULL;
    q->rear=NULL;

}

int push(que *q,int num)
{
     node *temp;
    temp=(struct node*)malloc(sizeof(node));
    temp->data=num;
    temp->next=NULL;

    if (q->rear == NULL)
    {
        q->front = q->rear = temp;
        printf("Elementas: %d iterptas\n", *temp);
    }
    else
    {
        q->rear->next=temp;
        q->rear=temp;
        printf("Elementas: %d iterptas\n", *temp);
    }
}
