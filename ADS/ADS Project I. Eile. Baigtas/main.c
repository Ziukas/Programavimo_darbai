#include <stdio.h>
#include <stdlib.h>
#include "header.h"


int main(){
    que *q=init();
    printf("===================================================================\n");
    printf("*********************************ADS*******************************\n");
    printf("===================================================================\n");
    while (1){
    int ch;
    printf("Pasirinkite is funkciju apacioje:\n");
    printf("\t [1] Sukurti tuscia eile\n");
    printf("\t [2] Patikrinti ar eile tuscia ar pilna\n");//!!!!!!!!!!!!
    printf("\t [3] Ideti nauja elementa\n");
    printf("\t [4] Isimti elementa is eiles\n");
    printf("\t [5] Gauti pirmo eiles elemento duomenis\n");
    printf("\t [6] Gauti eiles elementu skaiciu\n");
    printf("\t [7] Istrinti visa eile\n");
    printf("\t [8] Spausdinti eile\n");
    printf("\t [0] Iseiti is programos\n");
    printf("***Iveskite pasirinkima:***\n\t");
        scanf("%d", &ch);
        if (ch==0) break;
        switch(ch){
            case 1:{init();break;}
            case 2:{ifFull(q); break;}
            case 3:{
                int n;
                printf("Iveskite koki elementa norite prideti: \n");
                scanf("%d", &n);
                push(q, n);
                break;}
            case 4:{deque(q); break;}
            case 5:{firstElement(q); break;}
            case 6:{size(q); break;}
            case 7:{deleteQ(q); break;}
            case 8:{print(q); break;}
    default:
        printf("!!!Whoops! Blogai ivedei pasirinkima, bandyk is naujo !!!\n");
        printf("\n");
        break;
    }}

return 0;
}
