#ifndef HEADER_H_INCLUDED
#define HEADER_H_INCLUDED



typedef struct Node {
    int data;
    struct node *next;
}node;

typedef struct Que
{
    struct Node *front, *rear;
}que;



que *init();                      // Sukuria tuscia eile
int firstElement();              //Parodo pirma elementa, jei eile tuscia, raso "eile tuscia"
int ifFull(que *q);              //Patikrina ar eile tuscia, jei eile tuscia, ta ir paraso, jei ne, paraso kad eile ne tuscia
int push(que *q,int num);        //Ideda elementa i eiles gala
int enque(que *q);               //Isima elementa is eiles pradzios, jei nera elementu, apie tai pranesa
int print(que *q);               // atspausdina eile, jei eiles tuscia, ta ir paraso
int size(que *q);                //Suranda eiles dydi, jei eile tuscia, ta ir grazina
int deleteQ(que *q);             //Atlaisvina atminti




#endif // HEADER_H_INCLUDED
