#include <iostream>
#include <stdlib.h>
#include <fstream>
using namespace std;
#define N 9


int kiekB;
int kiekJ;

char Lenta[N][N];
int pazymet[N][N];
int u=0;
void Init();
void print();
void print2();
void Init2();
void vieta(int i, int j);
void Juodi(int zirgoNr, int x, int y);
void Balti(int zirgoNr, int x, int y);


void Skaitymas(int &a, int &b){

    ifstream fd("duomenys.txt");
        fd>>a>>b;
    fd.close();
    kiekB=a;
    kiekJ=b;
    cout<<"Su "<<kiekJ+kiekB<<" zirgais"<<endl;
}

int main()
{
    Skaitymas(kiekB,kiekJ);
    Init();
    Balti(0,0,0);
    return 0;
}

void Balti(int zirgoNr, int x, int y)
{
    if (zirgoNr==kiekB)                                       //jei 6 zirgai
    {
        Init2();                                        //Uzpildo pazymejimu masyva
        int kiek=0;
        for (int i = 1; i<N-1; i++){                     //Bega per visa lenta ir iesko kuria langeliai uzimti
            for (int j = 1; j<N-1; j++){
                if (Lenta[i][j]=='B')
                    {vieta(i,j);}}
        }
        for(int i = 0;i < N; i++){
            for(int j = 0;j < N; j++)
                if(pazymet[i][j] == 1)
                    kiek++;}

        if(kiek>=41){
            Juodi(0,0,0);
            }         //Jei jau yra suzymeti 32 ar daugiau langeliu, tada eina skaiciuoti juodu
        return;
    }
    int nX=0, nY=0;                        //Kur naujas ejimas bus, nuo
    if(x == N-1 && y == N-1) return;       //Jei prieina lentos apatini desini kampa grizta
    if(x == N-1)  {nX = 0; nY = y + 1;}       //jei priejo x gala, tada paslenka y'u asimi i apacia ir grizta prie lentos x=1 langelio
    else {nX = x + 1; nY = y;}              // paeina vienu laukeliu i sona
    Balti(zirgoNr, nX, nY);                // eina su nauja koordinate
    if((x + y) % 2 == 0 && x >= 0 && x< N-1 && y >= 0 && y < N-1){//%0, nes baltu langeliu kordinaciu suma = 0, tai patikrina ar baltas langelis ir ar telpa i ribas x ir y

        Lenta[x][y] = 'B';               //Jei tenkina salyga, vietoj '0', istato zirga 'B' ant balto langelio

        Balti(zirgoNr+1, nX, nY);        //Jei telpa, eina su kitu zirgu

        Lenta[x][y] = 'O';


    }



}
void Juodi(int zirgoNr, int x, int y)
{
    if (zirgoNr==kiekJ)                                  //jei 6 zirgai
    {
        int kiek=0;
        Init2();
        for (int i = 0; i<=N; i++)                     //Bega per visa lenta ir iesko kuria langeliai uzimti
        {
            for (int j = 0; j<=N; j++)
            {
                if (Lenta[i][j]=='J' || Lenta[i][j]=='B' )
                    vieta(i,j);
            }
        }
        for (int i = 0; i<N; i++)                    //Skaiciuoja kiek langeliu jau uzimta
        {
            for (int j = 0; j<N; j++)
            {
                if(pazymet[i][j]==1) kiek++;
            }
        }
        if(kiek==81)
            {print(); exit(1);}          //Jei jau yra suzymeti 64 langeliai, tada eina spausdinti lentos
        return;
    }
    int nX=0, nY=0;
    if(x == N && y == N) return;
    if(x == N) {nX = 0; nY = y + 1;}
    else {nX = x + 1; nY = y;}
    Juodi(zirgoNr, nX, nY);
    if((x + y) % 2 == 1 && x >= 0 && x < N-1 && y >= 0 && y < N-1){
        Lenta[x][y] = 'J';
        Juodi(zirgoNr+1, nX, nY);
        Lenta[x][y] = 'O';
    }

}

void Init()                                       //Uzpildo lenta 'O' simboliais
{
    int x=0;
    for (int i =0; i<N; i++)
        for (int j = 0; j<N; j++)
        {
            Lenta[i][j]='O';
        }
}
void Init2()
{
    for (int i =0; i<N; i++)                      //Uzpildo nepazymetu langeliu masyva
        for (int j = 0; j<N; j++)
        {
            pazymet[i][j]=0;
        }
}

void print(){
    //cout<<Lenta[2][1]<<endl;
    for(int i = 0;i < N;i++){
        for(int j = 0;j < N;j++){
            if(Lenta[i][j] != 'J') cout <<Lenta[i][j]<<" ";
            else cout << Lenta[i][j] << " ";
        }
        cout << endl;
    }
    cout << endl;
}




void vieta(int i, int j)
{
        pazymet[i][j]=1;                       //Paduodamos koordinates kur yra zirgas, todel sis langelis uzimamas
    if(i+1<N && j+2<N && pazymet[i+1][j+2]==0)
        pazymet[i+1][j+2]=1;                  // Cia visur tikrina visus imanomus zirgo ejimus ir pakeicia langelio reiksme jei ant langelio galima atsistoti
    if(i+1<N && j-2>=0 && pazymet[i+1][j-2]==0)
        pazymet[i+1][j-2]=1;
    if(i+2<N && j+1<N && pazymet[i+2][j+1]==0)
        pazymet[i+2][j+1]=1;
    if(i+2<N && j-1>=0 && pazymet[i+2][j-1]==0)
        pazymet[i+2][j-1]=1;
    if(i-2>=0 && j+1<N && pazymet[i-2][j+1]==0)
        pazymet[i-2][j+1]=1;
    if(i-2>=0 && j-1>=0 && pazymet[i-2][j-1]==0)
        pazymet[i-2][j-1]=1;
    if(i-1>=0 && j+2<N && pazymet[i-1][j+2]==0)
        pazymet[i-1][j+2]=1;
    if(i-1>=0 && j-2>=0 && pazymet[i-1][j-2]==0)
        pazymet[i-1][j-2]=1;
}

