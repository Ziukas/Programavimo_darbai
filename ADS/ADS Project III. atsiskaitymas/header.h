#ifndef HEADER_H_INCLUDED
#define HEADER_H_INCLUDED

int darboPabaiga;
int darboPradzia;
int kiekPrideti;
int periodas;
int kaina;
int tikimybe1;
int tikimybe2;
int sugedes;

typedef struct Node {
    int data;
    struct node *next;
}node;

typedef struct Que
{
    struct Node *front, *rear;
}que;


typedef struct Que2
{
    struct Node *front, *rear;
}que2;


typedef struct Stack1
{
    struct Node *front;    //,*rear
}stack1;

typedef struct Stack2
{
    struct Node *front;    //,*rear
}stack2;

que *init();                      // Sukuria tuscia eile
int firstElement();              //Parodo pirma elementa, jei eile tuscia, raso "eile tuscia"
int ifFull(que *q);              //Patikrina ar eile tuscia, jei eile tuscia, ta ir paraso, jei ne, paraso kad eile ne tuscia
int pushStack(que *q,int num);        //Ideda elementa i eiles gala
int pushq(que *q, int num);
int deque(que *q, int xs);               //Isima elementa is eiles pradzios, jei nera elementu, apie tai pranesa
int print(que *q);               // atspausdina eile, jei eiles tuscia, ta ir paraso
int size(que *q);                //Suranda eiles dydi, jei eile tuscia, ta ir grazina
int deleteQ(que *q);             //Atlaisvina atminti, antras kintamasis kad butu aisku kuri eile
int ismesti(que *q);
int randomize();





#endif // HEADER_H_INCLUDED
