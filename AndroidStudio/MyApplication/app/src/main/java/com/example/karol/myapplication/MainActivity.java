package com.example.karol.myapplication;

import android.icu.text.NumberFormat;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

/**
 * This app displays an order form to order coffee.
 */
public class MainActivity extends AppCompatActivity {
    int kiekB = 0;
    int kiekR = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**
     * This method is called when the order button is clicked.
     */
    public void submitOrder(View view) {

        kiekR=0;
        kiekB=0;
        String redT="Score: " + kiekR;
        String blueT="Score: " + kiekB;
        displayMessageR(redT);
        displayMessageB(blueT);
    }

    public void incrementR1(View view) {
        kiekR++;
        displayR(kiekR);
    }
    public void incrementR2(View view) {
        kiekR=kiekR+2;
        displayR(kiekR);
    }

    public void incrementR3(View view) {
        kiekR=kiekR+3;
        displayR(kiekR);
    }

    public void incrementB1(View view) {
        kiekB++;
        displayB(kiekB);
    }
    public void incrementB2(View view) {
        kiekB=kiekB+2;
        displayB(kiekB);
    }

    public void incrementB3(View view) {
        kiekB=kiekB+3;
        displayB(kiekB);
    }

    /**
     * This method displays the given quantity value on the screen.
     */
    private void displayR(int number) {
        TextView quantityTextView = (TextView) findViewById(R.id.Red);
        quantityTextView.setText("Score: " + number);
    }

    private void displayB(int number) {
        TextView quantityTextView = (TextView) findViewById(R.id.Blue);
        quantityTextView.setText("Score: " + number);
    }
/*    private void displayBlue(int number) {
        TextView quantityTextView = (TextView) findViewById(R.id.Blue);
        quantityTextView.setText("" + number);
    }*/
    /**
     * This method displays the given price on the screen.
     */
    private void displayMessageR(String message) {
        TextView priceTextView = (TextView) findViewById(R.id.Red);
        priceTextView.setText(message);
    }
    private void displayMessageB(String message) {
        TextView priceTextView = (TextView) findViewById(R.id.Blue);
        priceTextView.setText(message);
    }
}
