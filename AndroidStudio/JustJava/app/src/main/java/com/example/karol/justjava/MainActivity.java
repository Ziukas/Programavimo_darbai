package com.example.karol.justjava;


import android.icu.text.NumberFormat;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

/**
 * This app displays an order form to order coffee.
 */
public class MainActivity extends AppCompatActivity {
    int kiek =0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    /**
     * This method is called when the order button is clicked.
     */
    public void submitOrder(View view) {
        CheckBox krems = (CheckBox) findViewById(R.id.kremas);
        boolean kremuotas = krems.isChecked();
        CheckBox choco = (CheckBox) findViewById(R.id.sokoladas);
        boolean sokolo = choco.isChecked();
        EditText vards = (EditText) findViewById(R.id.vardas);
        String name = vards.getText().toString();
        int price = calculatePrice(kremuotas, sokolo);
        String tekstas = createOrderSummary(price, kremuotas, sokolo, name);/*
        String tekstas = price + "$ uz " +kiek+ " kavas";
        tekstas = tekstas + "\n Aciu kad pirkote";*/
        displayMessage(tekstas);
    }

    public void increment(View view) {
        kiek++;
        displayQ(kiek);
    }
    public void decrement(View view) {
        kiek--;
        displayQ(kiek);
    }


    private void displayQ(int numbers) {
        TextView quantityTextView = (TextView) findViewById(R.id.quantity_text_view);
        quantityTextView.setText("" + numbers);
    }

    private void displayMessage(String message) {
        TextView orderSummaryTextView = (TextView) findViewById(R.id.order_summary_text_view);
        orderSummaryTextView.setText(message);
    }


    private int calculatePrice(boolean busena, boolean busena2)
    {
        int price = 5;
        if (busena)
            price++;
        if (busena2)
            price +=2;


        return kiek * price;
    }

    private String createOrderSummary(int price, boolean busena, boolean busena2, String name){
        String msg = "Name:" + name + "\n";
        msg+="Kiekis: " + kiek + "\n";
        msg+="Is viso: " + price + "\n";
        msg+="Ar su kremu: "  + busena + "\n";
        msg+="Ar su sokoladu: "  + busena2 + "\n";
        msg+="Aciu kad pirkote";
        return msg;

    }
}
