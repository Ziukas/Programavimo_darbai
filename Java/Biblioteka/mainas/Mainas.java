// =========================================================================
//
//  Kurso "Objektinis programavimas" (PS) 2016/17 m.m. pavasario (2) sem.
//   3-�ias praktinis darbas, 3-�ias variantas 
//  Darb� atliko: Karolis �i�kas PS 6 grup�, II Pogrupis
//	
// =========================================================================
//
//  Informacin� sistema pasirenkama savo nuo�i�ra. Informaciniai objektai apra�antys �i� sistem� ir veiksmai su jais.
//
//		3. Biblioteka: personalas, knyg� katalogas, skaitytojai;
// =========================================================================


package mainas;

import java.util.Scanner;

import javax.swing.plaf.synth.SynthScrollPaneUI;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import javax.swing.*;

import LibFailai.Book;

import LibFailai.Library;
import LibFailai.Reader;
import LibFailai.ReaderManage;
import LibFailai.Skolos;
import LibFailai.Worker;
import LibFailai.workerMgr;
import LibFailai.except;
import LibFailai.workerMgr;




/**
 * 
 * Pagrindine programos klas�, kurioje
 *  sukuriami bibliotekos ir skaitytoj� valdymo
 * Taip pat yra klonuojamas darbutoj� valdymo objektas
 * @author karol
 *
 */
public class Mainas implements Cloneable {

	static ReaderManage read = new ReaderManage();
	Scanner in = new Scanner(System.in);
	String vieta = in.nextLine();

	static Library lib = new Library("Vilniaus gatv�", 5, 18, 1997, 2000);

	static workerMgr darb = new workerMgr(1);

	// Klonavimas
	/**
	 * klonuojamas objektas
	 */
	static workerMgr darb2 = (workerMgr) darb.clone();

	/**
	 * 
	 * 
	 * Nuskaito darbuotoj� duomenis, kurie yra naudojami 
	 * darb.arYraPass metode, kuris tikrina ar vartojo �vesti duomenys 
	 * sutampa su esamais duomenimis
	 * @param args main metodo argumentas
	 * @throws except Jei neranda faila, meta exception
	 * 
	 * 
	 */

	public static void main(String[] args) throws except {

		javax.swing.SwingUtilities.invokeLater(new Runnable() {

			/**
			 * 
			 * 
			 * Nuskaito darbuotoj� duomenis, kurie yra naudojami 
			 * darb.arYraPass metode, kuris tikrina ar vartojo �vesti duomenys 
			 * sutampa su esamais duomenimis
			 * 
			 * 
			 */
			@Override
			public void run() {
				darb.addWorkers();
			}
		});

		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				Login();
			}
		});

	}

	/**
	 * Prideda skola skaitytojui, ivedus knygos pavadinima, skaitytojo pavarde ir ID
	 * @param title knygos pavadinimas
	 * @param surname skaitytojo pavarde
	 * @param n skaitytojo ID
	 */
	public static void addSkola(String title, String surname, int n) {

		if (lib.tikrinam(title) == true && read.tikrinam(surname, n) == true) {
			System.out.println("Knyga prid�ta");
			Skolos d = new Skolos(title, surname, n);
			lib.pakeiciam(false);
			lib.skolinames(d);
		} else {
			System.out.println("Tokios knygos n�ra");
		}
	}

	/**
	 * Panaikina skola skaitytojui, ivedus knygos pavadinima, skaitytojo pavarde ir ID
	 * @param title knygos pavadinimas
	 * @param surname skaitytojo pavarde
	 * @param n skaitytojo ID
	 */
	public static void removeSkola(String title, String surname, int n) {

		if (lib.tikrinam(title) == false && read.tikrinam(surname, n) == true) {
			System.out.println("Knyga prid�ta");
			Skolos d = new Skolos(title, surname, n);
			lib.pakeiciam(true);
			lib.grazinam(n);
		} else {
			System.out.println("Tokios knygos n�ra");
		}
	}

	/**
	 * Laukia, kol vartotojas �ves savo prisijungimo duomenis,
	 * nuspaudus mygtuk�, lyginima informacija su jau u�krauta i� failo
	 * jei vartotojo �vesti teisingi, galima t�sti darb� toliau
	 * jei ne, pra�oma �vesti duomenis i� naujo
	 */
	public static void Login() {
		JFrame frame = new JFrame("Biblioteka");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel contentPane = new JPanel();
		contentPane.setOpaque(true);
		contentPane.setBackground(Color.WHITE);
		contentPane.setLayout(null);

		JLabel label = new JLabel("Sveiki atvyke", JLabel.CENTER);
		label.setSize(300, 30);
		label.setLocation(145, 5);
		label.setFont(new Font("Courier New", Font.ITALIC, 20));

		JButton button = new JButton("Prisjungti");
		button.setSize(100, 30);
		button.setLocation(245, 130);

		JLabel label2 = new JLabel("ID:");
		label2.setSize(300, 30);
		label2.setLocation(145, 25);
		label2.setFont(new Font("Courier New", Font.ITALIC, 12));

		JTextField field = new JTextField("0000");
		field.setSize(300, 30);
		field.setLocation(145, 45);

		JLabel label3 = new JLabel("Slapta�odis:");
		label3.setSize(300, 30);
		label3.setLocation(145, 68);
		label3.setFont(new Font("Courier New", Font.ITALIC, 12));

		JTextField field2 = new JTextField("0000");
		field2.setSize(300, 30);
		field2.setLocation(145, 90);

		contentPane.add(label);
		contentPane.add(button);
		contentPane.add(field);
		contentPane.add(field2);
		contentPane.add(label2);
		contentPane.add(label3);
		frame.setContentPane(contentPane);
		frame.setSize(600, 400);
		frame.setLocationByPlatform(true);
		frame.setVisible(true);

		frame.setLocationRelativeTo(null);

		frame.setVisible(true);

		button.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent ae) {
				String text = field.getText();
				try{
				int id = Integer.valueOf(text);
				

				String text2 = field2.getText();
				if (darb.arYraPass(id, text2)) {
					frame.setVisible(false);
					javax.swing.SwingUtilities.invokeLater(new Runnable() {
						@Override
						public void run() {
							read.addReaders();
							lib.addKnygos();
							lib.addSkolos();

						}
					});

					Menu();
				} else {
					label.setSize(400, 30);
					label.setFont(new Font("Courier New", Font.ITALIC, 12));
					label.setText("Blogi prisijungimo duomenys");
				}
				}
				catch(Exception c){
					System.out.println("Blogai ivestas id, bandykite is naujo");
					System.out.println(c.getMessage());
				}
			}
		});

	}

	/**
	 * Pagrindinis programos meniu
	 */
	public static void Menu() {
		// Create and set up the window.
		JFrame frame = new JFrame("Biblioteka");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel contentPane = new JPanel();
		contentPane.setOpaque(true);
		contentPane.setBackground(Color.WHITE);
		contentPane.setLayout(null);

		JLabel label = new JLabel("MENU", JLabel.CENTER);
		label.setSize(300, 30);
		label.setLocation(145, 5);
		label.setFont(new Font("Courier New", Font.ITALIC, 20));

		JLabel labelKnygu = new JLabel("Knygos", JLabel.CENTER);
		labelKnygu.setSize(50, 30);
		labelKnygu.setLocation(100, 25);
		labelKnygu.setFont(new Font("Courier New", Font.ITALIC, 12));

		// Knygos
		JButton prideti = new JButton("Prideti");
		prideti.setSize(100, 30);
		prideti.setLocation(100, 50);
		JButton spausd = new JButton("Spausdinti");
		spausd.setSize(100, 30);
		spausd.setLocation(200, 50);
		JButton salinti = new JButton("�alinti");
		salinti.setSize(100, 30);
		salinti.setLocation(300, 50);

		// READERS
		JLabel labelSkai = new JLabel("Skaitytojai");
		labelSkai.setSize(100, 30);
		labelSkai.setLocation(100, 75);
		labelSkai.setFont(new Font("Courier New", Font.ITALIC, 12));
		JButton pridetiS = new JButton("Prid�ti");
		pridetiS.setSize(100, 30);
		pridetiS.setLocation(100, 100);
		JButton spausdS = new JButton("Spausdinti");
		spausdS.setSize(100, 30);
		spausdS.setLocation(200, 100);
		JButton salintiS = new JButton("�alinti");
		salintiS.setSize(100, 30);
		salintiS.setLocation(300, 100);

		// Knygos skolinimas, grazinimas
		JLabel labelSkol = new JLabel("Skolos");
		labelSkol.setSize(100, 30);
		labelSkol.setLocation(100, 125);
		labelSkol.setFont(new Font("Courier New", Font.ITALIC, 12));

		JButton spausdSk = new JButton("Spausdinti");
		spausdSk.setSize(100, 30);
		spausdSk.setLocation(100, 150);

		JButton paimti = new JButton("Paimti");
		paimti.setSize(100, 30);
		paimti.setLocation(200, 150);

		JButton grazinti = new JButton("Gr��inti");
		grazinti.setSize(100, 30);
		grazinti.setLocation(300, 150);

		// Saugojimas
		JLabel labelEnd = new JLabel("I�saugoti ir i�eiti");
		labelEnd.setSize(150, 30);
		labelEnd.setLocation(100, 175);
		labelEnd.setFont(new Font("Courier New", Font.ITALIC, 12));

		JButton save = new JButton("Saugoti i faila");
		save.setSize(100, 30);
		save.setLocation(100, 200);

		contentPane.add(label);
		contentPane.add(prideti);
		contentPane.add(spausd);
		contentPane.add(salinti);
		contentPane.add(pridetiS);
		contentPane.add(spausdS);
		contentPane.add(salintiS);
		contentPane.add(paimti);
		contentPane.add(labelSkai);
		contentPane.add(labelSkol);
		contentPane.add(grazinti);
		contentPane.add(labelEnd);
		contentPane.add(labelKnygu);
		contentPane.add(save);

		contentPane.add(spausdSk);
		frame.setContentPane(contentPane);
		frame.setSize(600, 600);
		frame.setLocationByPlatform(true);
		frame.setVisible(true);

		frame.setLocationRelativeTo(null);

		frame.setVisible(true);

		prideti.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent ae) {
				// lib.addBook();
				// System.out.println(lib.toString());
				// System.out.println(text2);
				frame.setVisible(false);
				knygosPridejimas();
				// frame.dispatchEvent(new WindowEvent(frame,
				// WindowEvent.WINDOW_CLOSING));
			}
		});

		spausd.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent ae) {
				lib.papildytiLista();
				System.out.println(lib.toString());
				frame.setVisible(false);

				frame.setVisible(true);

			}
		});

		spausdSk.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent ae) {
				System.out.println(lib.toString(true));
				lib.papildytiLista2();
				frame.setVisible(false);

				frame.setVisible(true);

			}
		});

		salinti.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent ae) {
				// lib.papildytiLista();
				knygosSalinimas();
				// System.out.println(lib.toString());
				frame.setVisible(false);

			}
		});

		pridetiS.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent ae) {

				frame.setVisible(false);
				skaitytojoPridejimas();

			}
		});

		paimti.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent ae) {

				skolosPridejimas();
				frame.setVisible(false);

			}
		});

		grazinti.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent ae) {

				skolosTrinimas();
				frame.setVisible(false);

			}
		});

		save.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent ae) {
				lib.safeSkolos();
				lib.safe();
				read.safe();
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
				frame.setVisible(false);
				// frame.setVisible(false);
			}
		});

		salintiS.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent ae) {
				// lib.papildytiLista();
				skaitytojoSalinimas();
				// System.out.println(lib.toString());
				frame.setVisible(false);

			}
		});

		spausdS.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent ae) {
				read.papildytiLista();
				System.out.println(read.toString());
				// lib.addBook();
				// System.out.println(text2);
				frame.setVisible(true);

			}
		});

	}

	/**
	 * Meniu, skirtas knygos �alinimui i� bibliotekos
	 */
	public static void knygosSalinimas() {
		JFrame frame = new JFrame("Salinimas");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel contentPane = new JPanel();
		contentPane.setOpaque(true);
		contentPane.setBackground(Color.WHITE);
		contentPane.setLayout(null);

		JLabel labelSkai = new JLabel("Knygos Indexas");
		labelSkai.setSize(100, 30);
		labelSkai.setLocation(145, 25);
		labelSkai.setFont(new Font("Courier New", Font.ITALIC, 12));

		JButton saugoti = new JButton("�alinti");
		saugoti.setSize(100, 30);
		saugoti.setLocation(250, 100);

		JTextField index = new JTextField("0");
		index.setSize(300, 30);
		index.setLocation(145, 45);

		contentPane.add(saugoti);
		contentPane.add(index);
		contentPane.add(labelSkai);
		frame.setContentPane(contentPane);
		frame.setSize(600, 400);
		frame.setLocationByPlatform(true);
		frame.setVisible(true);

		frame.setLocationRelativeTo(null);

		frame.setVisible(true);

		saugoti.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent ae) {

				String text3 = index.getText();
				try {
				int id = Integer.valueOf(text3);
				
					lib.remove(id);
				}				
				catch(NumberFormatException o){
					System.out.println("Blogai ivestas skaicius, knyga neistrinta");
					
				} 
				catch (Exception e) {
					System.out.println(e.getMessage());
				}
				Menu();
				frame.setVisible(false);

			}
		});

	}

	/**
	 * Meniu skirtas pa�alinti skaitytoj� i� bibliotekos sistemos
	 */
	public static void skaitytojoSalinimas() {
		JFrame frame = new JFrame("Biblioteka");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel contentPane = new JPanel();
		contentPane.setOpaque(true);
		contentPane.setBackground(Color.WHITE);
		contentPane.setLayout(null);

		JLabel labelSkai = new JLabel("Skaitytojo Indexas");
		labelSkai.setSize(200, 30);
		labelSkai.setLocation(145, 25);
		labelSkai.setFont(new Font("Courier New", Font.ITALIC, 12));

		JButton saugoti = new JButton("�alinti");
		saugoti.setSize(100, 30);
		saugoti.setLocation(250, 90);

		JTextField index = new JTextField("0");
		index.setSize(300, 30);
		index.setLocation(145, 45);

		contentPane.add(saugoti);
		contentPane.add(index);
		contentPane.add(labelSkai);
		frame.setContentPane(contentPane);
		frame.setSize(600, 400);
		frame.setLocationByPlatform(true);
		frame.setVisible(true);

		frame.setLocationRelativeTo(null);

		frame.setVisible(true);

		saugoti.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent ae) {

				String text3 = index.getText();
				// text3= " " + text3 + " ";
				try{
				int id = Integer.valueOf(text3);
				read.remove(id);
				} 
				catch(NumberFormatException o){
					System.out.println("Blogai ivestas skaicius, skaitytojas neistrintas");
					
				}
				catch (Exception c) {
					System.out.println(c.getMessage());
				}
				Menu();
				frame.setVisible(false);

			}
		});

	}

	/**
	 * Meniu, skirtas prid�ti nauj� knyg� � sistem�
	 */
	public static void knygosPridejimas() {
		// Create and set up the window.
		JFrame frame = new JFrame("Biblioteka");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel contentPane = new JPanel();
		contentPane.setOpaque(true);
		contentPane.setBackground(Color.WHITE);
		contentPane.setLayout(null);

		JLabel label = new JLabel("Knygos pridejimas", JLabel.CENTER);
		label.setSize(300, 30);
		label.setLocation(145, 5);
		label.setFont(new Font("Comic Sans", Font.ITALIC, 20));

		JButton saugoti = new JButton("Prid�ti");
		saugoti.setSize(100, 30);
		saugoti.setLocation(250, 180);

		JTextField pav = new JTextField("Pavadinimas");
		pav.setSize(300, 30);
		pav.setLocation(145, 45);

		JTextField aut = new JTextField("Autorius");
		aut.setSize(300, 30);
		aut.setLocation(145, 90);

		JTextField metai = new JTextField("2016");
		metai.setSize(300, 30);
		metai.setLocation(145, 135);

		contentPane.add(label);
		contentPane.add(saugoti);
		contentPane.add(pav);
		contentPane.add(aut);
		contentPane.add(metai);

		frame.setContentPane(contentPane);
		frame.setSize(600, 400);
		frame.setLocationByPlatform(true);
		frame.setVisible(true);

		// center the jframe on screen
		frame.setLocationRelativeTo(null);

		frame.setVisible(true);

		saugoti.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent ae) {

				String text = pav.getText();
				text = " " + text + " ";
				String text2 = aut.getText();
				text2 = " " + text2 + " ";
				String text3 = metai.getText();
				// text3= " " + text3 + " ";
				try{
				int year = Integer.valueOf(text3);
				lib.pridetiUi(text, text2, year);}
				catch(Exception c){
					System.out.println("Blogai ivestas skaicius, knyga neprideta");
					System.out.println(c.getMessage());
				}
				frame.setVisible(false);

				Menu();
			}
		});

	}

	/**
	 * Meniu, skirtas prid�ti nauj� skaityoj� � sistem�
	 */
	public static void skaitytojoPridejimas() {
		// Create and set up the window.
		JFrame frame = new JFrame("Biblioteka");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel contentPane = new JPanel();
		contentPane.setOpaque(true);
		contentPane.setBackground(Color.WHITE);
		contentPane.setLayout(null);

		JLabel label = new JLabel("Skaitytojo prid�jimas", JLabel.CENTER);
		label.setSize(300, 30);
		label.setLocation(145, 5);
		label.setFont(new Font("Comic Sans", Font.ITALIC, 20));

		JButton saugoti = new JButton("Prid�ti");
		saugoti.setSize(100, 30);
		saugoti.setLocation(250, 180);

		JTextField pavarde = new JTextField("Pavard�");
		pavarde.setSize(300, 30);
		pavarde.setLocation(145, 45);

		JTextField id = new JTextField("4545");
		id.setSize(300, 30);
		id.setLocation(145, 135);

		contentPane.add(label);
		contentPane.add(saugoti);
		contentPane.add(pavarde);
		contentPane.add(id);

		frame.setContentPane(contentPane);
		frame.setSize(600, 400);
		frame.setLocationByPlatform(true);
		frame.setVisible(true);

		frame.setLocationRelativeTo(null);

		frame.setVisible(true);

		saugoti.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent ae) {

				String text = pavarde.getText();
				text = " " + text + " ";
				String text3 = id.getText();
				// text3= " " + text3 + " ";
				try{
				int year = Integer.valueOf(text3);
				read.pridetiUi(text, year);}
				catch(Exception c){
					System.out.println("Blogai ivestas skaicius, skaitytojas nepridetas");
					System.out.println(c.getMessage());
				}
				frame.setVisible(false);

				Menu();
			}
		});

	}
	
	/**
	 * Skolos prid�jimo meniu
	 */
	public static void skolosPridejimas() {

		JFrame frame = new JFrame("Biblioteka");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel contentPane = new JPanel();
		contentPane.setOpaque(true);
		contentPane.setBackground(Color.WHITE);
		contentPane.setLayout(null);

		JLabel label = new JLabel("MENU", JLabel.CENTER);
		label.setSize(300, 30);
		label.setLocation(145, 5);
		label.setFont(new Font("Comic Sans", Font.ITALIC, 20));

		JButton saugoti = new JButton("Paimti");
		saugoti.setSize(100, 30);
		saugoti.setLocation(250, 180);

		JTextField pav = new JTextField("Pavadinimas");
		pav.setSize(300, 30);
		pav.setLocation(145, 45);

		JTextField aut = new JTextField("Skaitytojas");
		aut.setSize(300, 30);
		aut.setLocation(145, 90);

		JTextField id = new JTextField("0000");
		id.setSize(300, 30);
		id.setLocation(145, 135);

		contentPane.add(label);
		contentPane.add(saugoti);
		contentPane.add(pav);
		contentPane.add(aut);
		contentPane.add(id);

		frame.setContentPane(contentPane);
		frame.setSize(600, 400);
		frame.setLocationByPlatform(true);
		frame.setVisible(true);

		frame.setLocationRelativeTo(null);

		frame.setVisible(true);

		saugoti.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent ae) {

				String text = pav.getText();
				String text2 = aut.getText();
				String text3 = id.getText();
				try{
				int id = Integer.valueOf(text3);
				addSkola(text, text2, id);
				}
				catch(Exception c){
					System.out.println("Blogai ivestas skaicius, skola neprideta");
					System.out.println(c.getMessage());
				}
				frame.setVisible(false);
				Menu();
			}
		});

	}

	/**
	 * Skolos naikinimo meniu
	 */
	public static void skolosTrinimas() {

		JFrame frame = new JFrame("Biblioteka");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel contentPane = new JPanel();
		contentPane.setOpaque(true);
		contentPane.setBackground(Color.WHITE);
		contentPane.setLayout(null);

		JLabel label = new JLabel("MENU", JLabel.CENTER);
		label.setSize(300, 30);
		label.setLocation(145, 5);
		label.setFont(new Font("Comic Sans", Font.ITALIC, 20));

		JButton saugoti = new JButton("Gr��inti");
		saugoti.setSize(100, 30);
		saugoti.setLocation(250, 180);

		JTextField pav = new JTextField("Pavadinimas");
		pav.setSize(300, 30);
		pav.setLocation(145, 45);

		JTextField aut = new JTextField("Skaitytojas");
		aut.setSize(300, 30);
		aut.setLocation(145, 90);

		JTextField id = new JTextField("0000");
		id.setSize(300, 30);
		id.setLocation(145, 135);

		contentPane.add(label);
		contentPane.add(saugoti);
		contentPane.add(pav);
		contentPane.add(aut);
		contentPane.add(id);

		frame.setContentPane(contentPane);
		frame.setSize(600, 400);
		frame.setLocationByPlatform(true);
		frame.setVisible(true);

		frame.setLocationRelativeTo(null);

		frame.setVisible(true);

		saugoti.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent ae) {

				String text = pav.getText();
				String text2 = aut.getText();
				String text3 = id.getText();
				try{
				int id = Integer.valueOf(text3);
				removeSkola(text, text2, id);}
				catch(Exception c){
					System.out.println("Blogai ivestas skaicius, skola neistrinta");
					System.out.println(c.getMessage());
				}
				frame.setVisible(false);

				Menu();
			}
		});

	}



}
