//Abstrakti klase


package LibFailai;

/**
 * 
 * Abstrakti programos klas�, kuri implementuoja LibInterface.java interfeis� ir kuri i�ple�ia LibLaikas.java fail�
 * @author karol
 *
 */
public abstract class LibAbstract implements LibInterface {
	
	
	/**
	 * Gr��ina kiek daugiausia knyg� galima prid�ti � bibliotek�
	 * @return max knyg� skai�ius
	 */
	public abstract int displayMax();

	//Uzklotas metodas
	public String generateLaikas(){   
		return null;
	}


}
