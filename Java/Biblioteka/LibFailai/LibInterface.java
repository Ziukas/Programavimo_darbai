//Interfeisas

package LibFailai;

/**
 * 
 * Interfeisas, kuris apra�o LibLaikas.java klas� ir kur� extendina LibAbstract.java abstrakti klas�
 * @author karol
 *
 */
public interface LibInterface {
	
	
	/** 
	 * Gr��ina laik�
	 * @return laik�
	 */
	public String getLaikas();
	/**
	 * Gr��ina darbo laik� kaip vien� String'�
	 * @return laiko String�
	 */
	public String generateLaikas();
	/**
	 * Gra�ina kiek max knyg� galima pad�ti � bibliotek�
	 * @return max knyg� skai�i�
	 */
	public int getMaxKnygu();
	/**
	 * Gr��ina kuriais metais �kurta biblioteka
	 * @return metus
	 */
	public int getIkurta();
	/**
	 * Nustato max knyg� kiek�
	 * @param x knyg� kiekis
	 */
	public void setMaxKnygu(int x);
}
