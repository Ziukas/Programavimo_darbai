package LibFailai;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.util.*;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 * 
 * Sukuria knyg� bei skolinink� s�ra�as
 * @author karol
 *
 */
/**
 * Klas�, kurioje sukuriami atskiri knyg� bei skolinink� objektai, taip pat talpina informacija apie bibliotek� bei atliekamos
 * operacijos su knygomis ir skolininkais
 * @author karol
 *
 */
public class Library extends LibraryInfo {
	JFrame listFrame;
	Scanner x;
	private String info = "Biblioteka dirba";
	final String name = "Vilniaus miesto biblioteka"; // final panaudojimas
	boolean dirba = false;
	static int skolos = 0;
	/**
	 * Knygos b�sena
	 */
	public int yrad;
	static String knyguInfo = "index\tPavadinimas\tAutorius\tI�leidimo metai \tAr knyga bibliotekoje"; 
	
																										
	/**
	 * Knyg� s�ra�as
	 */
	private List<Book> collection;
	/**
	 * Skol� s�ra�as
	 */
	private List<Skolos> skolininkas;
	// DefaultListModel<Book> kolekcija;
	DefaultListModel<Book> kolekcija;
	DefaultListModel<Skolos> kolekcijas;// = new DefaultListModel<Skolos>();

	/**
	 * 
	 * Konstruktorius, kuris sukuria Bibliotekos informacijos objekt� ir knyg� bei skolinink� masyvus
	 * @param vieta bibliotekos vieta
	 * @param laikas1 darbo prad�ia
	 * @param laikas2 darbo pabaiga
	 * @param ikurta �k�rimo metai
	 * @param max did�iausias knyg� kiekis
	 */
	public Library(String vieta, int laikas1, int laikas2, int ikurta, int max) {

		super(vieta, laikas1, laikas2, ikurta, max); // Sukuria super klase
		collection = new ArrayList<Book>();
		skolininkas = new ArrayList<Skolos>();

	}

	// Knygu listas
	/**
	 * Sudeda knygas � list�, kuris spausdinamas vartotojo s�sajoje
	 */
	public void papildytiLista() {
		JFrame listFrame;
		kolekcija = new DefaultListModel<Book>();
		for (int i = 0; i < collection.size(); i++) {
			Book b = collection.get(i);
			kolekcija.addElement(b);
		}

		listFrame = new JFrame();
		JList<Book> myList = new JList<>(kolekcija);

		listFrame.add(new JScrollPane(myList));
		listFrame.add(myList);
		listFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		listFrame.setTitle("Knyg� s�ra�as");
		listFrame.setSize(600, 400);
		listFrame.setLocationRelativeTo(null);
		listFrame.setVisible(true);

	}

	// Skolu listas
	/**
	 * Sudeda skolinink� informacij� � Jlist'a, kuris v�liau naudojamas vartotojo s�sajoje
	 * 
	 */
	public void papildytiLista2() {
		JFrame listFrame;
		kolekcijas = new DefaultListModel<Skolos>();

		for (int i = 0; i < skolininkas.size(); i++) {
			// Book vn = new Book(" Vietnam ", " vn ", 444);
			Skolos b = skolininkas.get(i);
			kolekcijas.addElement(b);
		}

		listFrame = new JFrame();

		JList<Skolos> myList = new JList<>(kolekcijas);

		listFrame.add(new JScrollPane(myList));
		listFrame.add(myList);
		listFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		listFrame.setTitle("Skolinink� s�ra�as");
		listFrame.setSize(600, 400);
		listFrame.setLocationRelativeTo(null);
		listFrame.setVisible(true);

	}

	/**
	 * Prid�da nauj� knyg�
	 * @param title pavadinimas
	 * @param author autorius
	 * @param year i�leidimo metai
	 */
	public void pridetiUi(String title, String author, int year) {
		boolean yra = true;
		Book b = new Book(title, author, year, yra);
		collection.add(b);

	}

	/**
	 * Metodas, kuris i�trina knyg� i� sistemos naudodamas jos index'�
	 * @param x indeksas
	 * @throws except meta exception, jei index'as kur� �veda vartotojas yra per didelis
	 */
	public void remove(int x) throws except {

		if (x > collection.size() - 1 || x < 0) {
			throw (new except());
		}
		collection.remove(x);
		System.out.println("Knyga i�trinta");
	}

	public String toString() {

		String total = "\n";
		for (int i = 0; i < collection.size(); i++) {
			Book b = collection.get(i);
			total = total + i + " " + b.toString();
		}
		return total;
	}

	public String toString(boolean f) {

		String total = "\n";
		for (int i = 0; i < skolininkas.size(); i++) {
			Skolos b = skolininkas.get(i);
			total = total + i + " " + b.toString();
		}
		return total;
	}

//	// Overload
//	public void seek(String x) {
//		for (int i = 0; i < collection.size(); i++) {
//			if (collection.get(i).getTitle().equals(x)) {
//				System.out.println("Yra �ios knygos:" + collection.get(i).getTitle());
//			}
//		}
//	}
//
//	// OveLoad
//	public void seek(int u) {
//		for (int i = 0; i < collection.size(); i++) {
//			if (collection.get(i).getYear() == u) {
//				System.out.println("Yra �ios knygos:" + collection.get(i).getTitle());
//			}
//		}
//	}

//	public void paimti() { // pakeicia knygos busena i false
//		System.out.println("�veskite knygos pavadinim�");
//		Scanner input = new Scanner(System.in);
//		String title = input.nextLine();
//
//		for (int i = 0; i < collection.size(); i++) {
//			if (collection.get(i).getTitle().equals(title) && collection.get(i).getState() == true) {
//				collection.get(i).setState(false);
//				System.out.println("Knygos b�sena pakeista");
//
//				break;
//			}
//		}
//
//	}

//	public void grazinti() { // pakeicia knygos busena i true
//		System.out.println("�veskite knygos pavadinim�");
//		Scanner input = new Scanner(System.in);
//		String title = input.nextLine();
//		for (int i = 0; i < collection.size(); i++) {
//			if (collection.get(i).getTitle().equals(title) && collection.get(i).getState() == false) {
//				collection.get(i).setState(true);
//				break;
//			}
//		}
//	}

	/**
	 * 
	 * Prideda nauj� skolinink� 
	 * @param skola skolos objektas
	 */
	public void skolinames(Skolos skola) {
		skolininkas.add(skola);
	}

	/**
	 * Pa�alina skaitytojo skol�, kai knyga gr��inta
	 * @param id skaitytojo id
	 */
	public void grazinam(int id) {
		for (int i = 0; i < skolininkas.size(); i++) {
			if (skolininkas.get(i).getId() == id)
				skolininkas.remove(i);
		}

	}

	/**
	 * Tikrina, ar knyga dar nepaimta
	 * @param title knygos pavadinimas
	 * @return true arba false
	 */
	public boolean tikrinam(String title) { // patikrina ar knyga bibliotekoje
		for (int i = 0; i < collection.size(); i++) {
			if (collection.get(i).getTitle().equals(title) && collection.get(i).getState() == true) {
				yrad = i;
				return true;
			}

		}
		return false;

	}

	/** pakei�ia knygos b�sen�
	 * @param t b�sena
	 */
	public void pakeiciam(boolean t) {
		collection.get(yrad).setState(t);
	}



	/**
	 * 
	 * patikrina ar biblioteka dar ne pilna
	 * @return true or false
	 */ 
	public boolean checkMax() {
		if (collection.size() < super.getMaxKnygu()) {
			return true;
		} else {
			System.out.println("Knyg� limitas vir�ytas");
			return false;
		}
	}

	/**
	 * Nustato kiek daugiausia knyg� galima b�t� prid�ti
	 */
	public void setMaxKnygu() {
		System.out.println("�veskite maksimal� knyg� skai�i�");
		Scanner input = new Scanner(System.in);
		int x = input.nextInt();
		super.setMaxKnygu(x);
	}

	

	/**
	 * Prideda knygas � knyg� masyv�
	 */
	public void addKnygos() {
		open("knygos.txt");
		while (x.hasNext()) {
			String pav = x.next();
			String aut = x.next();
			int id = x.nextInt();
			boolean yra = x.nextBoolean();
			Book b = new Book(pav, aut, id, yra);
			collection.add(b);
		}

		close();

	}

	/**
	 * Prideda skol� � skolinink� masyv�
	 */
	public void addSkolos() {
		open("skolos.txt");
		while (x.hasNext()) {
			String pav = x.next();
			String vart = x.next();
			int id = x.nextInt();

			Skolos b = new Skolos(pav, vart, id);
			skolininkas.add(b);
		}

		close();

	}

	/**
	 * I�saugo skolas tekstiniame faile
	 */
	public void safeSkolos() {

		File pav = new File("skolos.txt");
		try {
			FileWriter fw = new FileWriter(pav);
			Writer output = new BufferedWriter(fw);
			int sz = skolininkas.size();
			for (int i = 0; i < sz; i++) {
				output.write(skolininkas.get(i).toString() + "\n");
			}
			output.close();

		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "kazkas negerai");
		}

	}



	/**
	 * 
	 * Atidaro fail�
	 * @param n failo pavadinimas
	 */
	public void open(String n) {
		try {

			x = new Scanner(new File(n));
		
		} catch (Exception e) {
			System.out.println("Failas nerastas");
		}
	}

	/**
	 * i�saugo knygas tekstiniame faile
	 */
	public void safe() {

		File pav = new File("knygos.txt");
		try {
			FileWriter fw = new FileWriter(pav);
			Writer output = new BufferedWriter(fw);
			int sz = collection.size();
			for (int i = 0; i < sz; i++) {
				output.write(collection.get(i).toString() + "\n");
			}
			output.close();

		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "kazkas negerai");
		}

	}

	
	/**
	 * U�daro fail�
	 */
	public void close() {
		x.close();
	}

}
