package LibFailai;


/**
 * Klas�, kurioje kuriama kiekviena knyga kaip atskiras objektas
 * @author karol
 *
 */
public class Book  {
	
	public String title;
	private String author;
	private int year;
	private boolean inLib;
	boolean yra;
	static int keista=0;
	
	
	
	/**
	 * @param title knygos pavadinimas
	 * @param author knygoa autorius
	 * @param year knygos i�leidimo metai
	 * @param yra knygos b�sena, ar ji yra bibliotekoje
	 */
	public Book(String title, String author, int year, boolean yra){
		this.title=title;
		this.author=author;
		this.year=year;
		this.yra = yra;
	}
	@Override
	public String toString(){
		return " " +title + " " + author + " " +year +  " " + yra +"\n";
	}
		
	/** 
	 * gr��in� knygos autori�
	 * @return autoriu
	 */
	public String getAuthor(){                        //get metodai
		return author;	
	}
	
	/** 
	 * gr��ina knygos pavadinim�
	 * @return knygos pavadinima
	 */
	public String getTitle(){
		return title;	
	}
	
	
	
	/** 
	 * gr��ina knygos i�leidimo metus
	 * @return knygos i�leidimo metus
	 */
	public int getYear(){
		return year;	
	}	
	
	
	/** gr��ina knygos b�sena - ar ji yra, ar ne bibliotekoje
	 * @return knygos b�sena
	 */
	public boolean getState(){
		return yra;
	}
	


	
	/** 
	 * Pakei�ia knygos b�sen�
	 * @param x knygos b�sena
	 */
	public void setState(boolean x){               //set metodas, pakeicia knygos busena
		yra=x;
		keista++;
	}

	

}
