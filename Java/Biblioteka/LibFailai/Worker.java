package LibFailai;



/**
 * Darbuotoj� klas�
 * @author karol
 *
 */
public class Worker {
	
	private String name;
	private String surname;
	private int id;
	private String pass;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Sukuria darbuotoj� su duotu vardu, pavarde, ID ir slapta�od�iu, kuris naudojamas prisijungiant
	 * @param name vardas
	 * @param surname pavard�
	 * @param id ID
	 * @param pass slapta�odis
	 */
	public Worker(String name, String surname, int id, String pass){
		
		this.name=name;
		this.surname=surname;
		this.id=id;
		this.pass=pass;
		
	}
	
	public String toString(){
		
		return name + "\t\t" + surname + "\t\t" +id + "\t\t" + "\n";
		
	}
	
	public String getPass(){
		return pass;
	}
	public void setPass(String x){
		pass = x;
	}
	

	

}
