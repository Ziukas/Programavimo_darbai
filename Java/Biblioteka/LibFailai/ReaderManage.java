package LibFailai;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.util.*;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;



/**
 * Klas�, skirta valdyti skaitytojus
 * @author karol
 *
 */
public class ReaderManage {
	DefaultListModel<Reader> kolekcija;

	Scanner x;
	
	/**
	 * Skitytoj� s�ra�as
	 */
	public List<Reader> readers;

	/**
	 * Konstruktorius skirtas skaitytoj� masyvui sukur
	 */
	public ReaderManage() {

		readers = new ArrayList<Reader>();

	}

	/**
	 * Prideda skaitytojus � Jlist'a, kuris spausdina skaitytojus vartojojo s�sajoje
	 */
	public void papildytiLista() {
		
		JFrame listFrame;
		kolekcija = new DefaultListModel<Reader>();
		System.out.println(readers.size());
		// System.out.println(toString());
		for (int i = 0; i < readers.size(); i++) {
			Reader b = readers.get(i);
			kolekcija.addElement(b);
		}

		listFrame = new JFrame();
		JList<Reader> myList = new JList<>(kolekcija);
		listFrame.add(new JScrollPane(myList));
		listFrame.add(myList);
		listFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		listFrame.setTitle("Skaitytoj� s�ra�as");
		listFrame.setSize(600, 400);
		listFrame.setLocationRelativeTo(null);
		listFrame.setVisible(true);
	}




	/**
	 * Prideta skaitytojus su gautais duomenimis
	 * @param pavard pavard�
	 * @param id ID
	 */
	public void pridetiUi(String pavard, int id) {
		Reader b = new Reader(pavard, id);
		readers.add(b);
		System.out.println("Knyga prid�ta");
	}

	/**
	 * Sukelia skaitytojus i� tekstinio failo
	 */
	public void addReaders() {
		open();
		while (x.hasNext()) {
			String surname = x.next();
			int id = x.nextInt();
		
			Reader b = new Reader(surname, id);
			readers.add(b);
		}
		close();

	}

	/**
	 * Atidaro tekstin� fail�
	 */
	public void open() {
		try {

			x = new Scanner(new File("skaitytojai.txt"));
			
		} catch (Exception e) {
			System.out.println("Failas nerastas");
		}
	}

	/**
	 * U�daro tekstin� fail�
	 */
	public void close() {

		x.close();
	}

	public String toString() {
		//System.out.println(detales);
		String total = "\n";
		for (int i = 0; i < readers.size(); i++) {
			Reader c = readers.get(i);
			total = total + i + "\t" + c.toString();
		}
		return total;
	}

	/**
	 * 
	 * Patikrina ar toks skaitytojas yra sistemoje
	 * @param surname pavard�
	 * @param x id
	 * @return true arba false
	 */
	public boolean tikrinam(String surname, int x) {
		for (int i = 0; i < readers.size(); i++)
			if (readers.get(i).getSurname().equals(surname) && readers.get(i).getId() == x) {
				return true;
			}
		return false;
	}

	// Isimties metimas
	/**
	 * I�trina vartojoj� pagal jo index'�
	 * @param x index'as
	 * @throws except kai �vedamas per didelis index'as
	 */
	public void remove(int x) throws except {

		if (x > readers.size() - 1 || x<0) {
			throw (new except());
		}
		readers.remove(x);
		System.out.println("Vartotojas i�trintas");
	}
	
	

	/**
	 * I�saugo informacij� tekstiniame faile
	 */
	public void safe() {

		File pav = new File("skaitytojai.txt");

		try {
			FileWriter fw = new FileWriter(pav);
			Writer output = new BufferedWriter(fw);
			int sz = readers.size();
			for (int i = 0; i < sz; i++) {
				output.write(readers.get(i).toString() + "\n");
			}
			output.close();

		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "kazkas negerai");
		}

	}
}
