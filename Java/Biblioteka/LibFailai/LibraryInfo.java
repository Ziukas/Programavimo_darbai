package LibFailai;

/**
 * 
 * 
 * Klas� skirta i�bandyti klasi� herarchijas
 * @author karol
 *
 */
public class LibraryInfo extends LibLaikas {

	private static String vieta;

	/**
	 * Konstruktorius, kuris pasinaudoja super klase sukurdamas objekt� skirt� informacijai apie bilbiotek�
	 * @param vieta bibliotekos vieta
	 * @param laikas1 darbo laikas
	 * @param laikas2 darbo laikas
	 * @param ikurta �k�rimo metai
	 * @param max max knyg� skai�ius
	 */
	public LibraryInfo(String vieta, int laikas1, int laikas2, int ikurta, int max) {
		super(laikas1, laikas2, ikurta, max);
		this.vieta = vieta;
	}

	/**
	 * Gr��ina bibliotekos adred�
	 * @return bibliotekos vieta
	 */
	public static String getVieta() { // Sitas metodas negali buti overridintas
		return vieta;
	}

	/**
	 * 
	 * Patikrina ar biblioteka gali dirbti duotu laiku
	 * @return true or false
	 */
	public boolean checkTime() {
		if (super.laikas1 < 6 || super.laikas2 > 22) {
			System.out.println("Biblioteka tokiu laiku dirbti negali");
			return false;
		}
		System.out.println("Biblioteka gali dirbti");
		return true;
	}

	@Override
	public String generateLaikas() { // Overridina metoda is super klases

		return "Darbo laikas " + super.laikas1 + "-" + super.laikas2;
	}

	@Override // Overridina dar viena metoda
	public int displayMax() {
		return super.getMaxKnygu();
	}

	/**
	 * Atspausdina informacija apie bibliotek� gaut� i� super klas�s
	 */
	public void displayInfo() {
		System.out.println("Bibliotekos vieta: " + vieta + " Darbo laikas: " + laikas1 + "-" + laikas2 + " �kurta: "
				+ super.getIkurta());
	}

	// public final void patvirtinimas(){ //negali uzkloti metodo is super
	// klases
	// System.out.println("Sitas metodas buvo uzklotas");
	// }

}
