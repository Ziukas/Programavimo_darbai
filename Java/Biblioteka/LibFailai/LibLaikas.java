package LibFailai;

/**
 * 
 * Sud�ta ne tokia svarbi bibliotekos informacija, taip pat, �i klas� priklauso nuo abstrak�ios klas�s ir interfeiso
 * @author karol
 *
 */
public class LibLaikas extends LibAbstract {

	public int laikas1;
	public int laikas2;
	private int ikurta;
	private int maxKnygu;
	String nej = "Darbo laikas" + ikurta + "yra";

	/**
	 * 
	 * Konstruktorius, kuris sukuria objekt�, kuris netrukdo Bibliotekos sistemai funkcionuoti
	 * @param laikas1 nuo kada atsidaro
	 * @param laikas2 kada u�sidaro
	 * @param ikurta kelintais metais �kurta biblioteka
	 * @param maxKnygu kiek max knyg� galima talpinti
	 */
	LibLaikas(int laikas1, int laikas2, int ikurta, int maxKnygu) {
		this.laikas1 = laikas1;
		this.laikas2 = laikas2;
		this.ikurta = ikurta;
		this.maxKnygu = maxKnygu;
		// patvirtinimas();
	}

	// public LibLaikas(int laikas1, int laikas2, int ikurta, int maxKnygu){
	// super(laikas1, laikas2, ikurta, maxKnygu);
	// }

	public String getLaikas() {
		return null;
	}

	//
	// public String generateLaikas(){ //uzklotas metodas
	// return null;
	// }
	public int getIkurta() {
		return ikurta;
	}

	public void setMaxKnygu(int x) {
		maxKnygu = x;

	}

	public int getMaxKnygu() {
		return maxKnygu;
	}

	public int displayMax() { // sitas metodas uzklotas
		return 0;
	}

	/**
	 * Spausdinamas tik tada, kai visa informacija sud�ta � metod�
	 */
	public final void patvirtinimas() { // Metodas kurio negalima uzkloti

		System.out.println("Informacija apie bibliotek� s�kmingai sud�ta");
	}

}
