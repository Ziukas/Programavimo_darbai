package LibFailai;


/**
 * 
 * Skolinink� klas�
 * @author karol
 *
 */
public class Skolos {
	
	String title;
	String surname;
	int id;
	
	/**
	 * Koks skaitytojas, koki� knyg� skolingas
	 * @param title knygos pavadinimas
	 * @param surname skaitytojo pavard�
	 * @param id ID
	 */
	public Skolos(String title, String surname, int id){
		this.title=title;
		this.surname=surname;
		this.id=id;
	}
	@Override
	public String toString(){
		
		return title + " " +surname + " "+ id + "\n";
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
}
