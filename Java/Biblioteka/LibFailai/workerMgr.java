package LibFailai;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;

import javax.swing.JOptionPane;

/**
 * Klas�, skirta tvarkyti darbuotojus
 * @author karol
 *
 */
public class workerMgr implements Cloneable, Runnable {

	int maxDarb;
	Scanner x;
	int darboVal = 8;
	int maxDarboVal = 10;
	private List<Worker> darbuotojas;

	/**
	 * Sukuria darbuotoj� masyv�
	 * @param maxDarb max darbuotoj� kiekis
	 */
	public workerMgr(int maxDarb) {
		this.maxDarb = maxDarb;
		darbuotojas = new ArrayList<Worker>();

	}

	public void run() {
		addWorkers();

	}

	/**
	 * Metodas, skirtas nuskanuoti darbuotojus is failo
	 * Darbuotoju duomenys po to naudojami norint prisijungti prie sistemos
	 */
	public void addWorkers() {
		open();
		while (x.hasNext()) {
			String name = x.next();
			String surname = x.next();
			int id = x.nextInt();
			String pass = x.next();

			Worker b = new Worker(name, surname, id, pass);
			darbuotojas.add(b);
		}
		// System.out.println(darbuotojas.size());

		close();

	}

	/**
	 * �alina darbuotojus naudojant j� index'�
	 * @param x index'as
	 */
	public void salinti(int x) {
		darbuotojas.remove(x);

	}

	/**
	 * Prideda darbutojus � sistem�
	 */
	public void pridetidarb() {
		Scanner inputas = new Scanner(System.in);
		System.out.println("�veskte darbuotojo pavard�");
		String vardas = inputas.nextLine();
		System.out.println("�veskte darbuotojo pavard�");
		String pavarde = inputas.nextLine();
		System.out.println("�veskite darbuotojo id i� 4 skaitmen�");
		int id = inputas.nextInt();
		System.out.println("�veskite darbuotojo Slaptazodi");
		String pass = inputas.nextLine();

		Worker c = new Worker(vardas, pavarde, id, pass);
		darbuotojas.add(c);
		System.out.println("Vartotojas prid�tas");
	}

	// issaugo i faila
	/**
	 * I�saugo darbuotjus ir j� informacij� tekstiniame faile
	 */
	public void safe() {

		File pav = new File("darbuotojai.txt");

		try {
			FileWriter fw = new FileWriter(pav);
			Writer output = new BufferedWriter(fw);
			int sz = darbuotojas.size();
			for (int i = 0; i < sz; i++) {
				output.write(darbuotojas.get(i).toString() + "\n");
			}
			output.close();

		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "kazkas negerai");
		}

	}

	// spausdinimas i konsole
	@Override
	public String toString() {
		System.out.println("DARBUOTOJAI:\n");
		String total = "\n";

		// System.out.println(darbuotojas.size());
		for (int i = 0; i < darbuotojas.size(); i++) {
			System.out.println("Kazkas yra");
			Worker b = darbuotojas.get(i);
			total = total + b.toString();
		}
		return total;
	}

	public Object clone() {
		try {
			return super.clone();
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Atidaro fail�
	 */
	public void open() {
		try {

			x = new Scanner(new File("darbuotojai.txt"));

		} catch (Exception e) {
			System.out.println("Failas nerastas");
		}
	}

	public void close() {

		x.close();
	}

	public int getMaxDarb() {
		return maxDarb;
	}

	public void setMaxDarb(int x) {
		maxDarb = x;
	}

	/**
	 * Patikrina, ar darbuotojai gali dirbti jiems paskirt� laik�
	 */
	public void patikrink() {
		if (darboVal > maxDarboVal) {
			System.out.println("Darbuotojai negal�s dirbti " + darboVal + " valandas(-�)");
			return;
		} else {
			System.out.println("Darbuotojai gal�s dirbti " + darboVal + " valandas(-�)");
			return;
		}
	}

	/**
	 * Nustato darbo valandas
	 */
	public void setDarboVal() {
		System.out.println("�veskite darbo valandas");
		Scanner in = new Scanner(System.in);
		int x = in.nextInt();
		darboVal = x;
	}

	public int getDarboVal() {
		return darboVal;
	}

	/**
	 * Patikrina, ar slapta�odis yra sistemoje
	 * @param id ID
	 * @param pass slapta�odis
	 * @return true or false
	 */
	public boolean arYraPass(int id, String pass) {
		for (int i = 0; i < darbuotojas.size(); i++) {
			if (darbuotojas.get(i).getId() == id && darbuotojas.get(i).getPass().equals(pass))
				return true;

		}
		return false;
	}

}
