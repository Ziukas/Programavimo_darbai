package LibFailai;

/**
 * 
 * Klas� skirta suskurti skaitytojo objekt� i� gautos informacijos
 * @author karol
 *
 */
public class Reader {

	public String surname;
	public int id;
	
	
	/**
	 * Sukuria skaitytojo objekt�
	 * @param surname pavard�
	 * @param id  vartotojo ID
	 */
	public Reader(String surname, int id){
		this.surname = surname;
		this.id = id;
	}
	
	//@Override
	public final String toString(){
		return surname + " " + id +"\n";
	}
	
	/**
	 * gr��ina vartotojo pavard�
	 * @return pavard�
	 */
	public String getSurname(){
		return surname;
	}
	
	/**
	 * Gr��ina vartotojo ID
	 * @return ID
	 */
	public int getId(){
		return id;
	}
}
