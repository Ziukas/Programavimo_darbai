@echo off
rem =========================================================================
rem
rem  Kurso "Objektinis programavimas" (PS) 2016/17 m.m. pavasario (2) sem.
rem  3-ečias praktinis darbas, 3-ečias variantas 
rem  Darbą atliko: Karolis Žiūkas PS 6 grupė, II Pogrupis
rem	
rem =========================================================================
rem
rem Informacinė sistema pasirenkama savo nuožiūra. Informaciniai objektai aprašantys šią sistemą ir veiksmai su jais.
rem
rem		3. Biblioteka: personalas, knygų katalogas, skaitytojai;
rem =========================================================================
rem Užduotis:

rem Sukurti (netuščią) interfeisą (ar panaudoti jau egzistuojantį - standartinį), 
rem išreiškiantį kurios nors parašytos klasės funkcionavimo aspektą. 
rem Interfeisą realizuoti abstrakčiąja klase, o pastarąją - konkrečiąja. 
rem Atkreipti dėmesį, kad konkrečiosios klasės funkcionalumo panaudojimas turi 
rem remtis kiek įmanoma abstraktesne klase (interfeisu).

rem Apibrėžti bazinę išimties klasę savo projektui. Apibrėžti išvestinę 
rem išimties klasę su patikslinančiąja informaciją. Kitos klasės metodams 
rem deklaruoti (throws) metamas išimtis ir esant neteisingam kreipiniui jas 
rem iššaukti (throw). Testinėje (main) programos klasėje gaudyti metamas išimtis, 
rem parūpinant vartotoją diagnostine informacija.
rem Parinkti bent vieną klasę, kurios objektą būtų prasminga klonuoti ir paruošti 
rem (giliam) klonavimui. Kitoje klasėje klonavimą prasmingai panaudoti.


rem Interfeisas yra LibFailai/LibInterface.java faile
rem Abstrakti klasė yra LibFailai/LibAbstract.java faile
rem Klasė, kuri remiasi abstrakčia klase ir interfeisu yra LibFailai/LibLaikas

rem Išimties klasė yra LibFailai/except.java. Išimtys metamos LibFailai/ReaderManager.java, LibFailai/Library.java failuose ir gaudomi mainas/Mainas.java faile
rem Klonuojamos klasės objektas yra LibFailai/workerMgr.java faile 


javac -encoding Cp1257 LibFailai/*.java mainas/*.java