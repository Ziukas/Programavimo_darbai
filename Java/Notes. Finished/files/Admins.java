package files;
public class Admins {
	
	private int id;
	private String pass;
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}


	public Admins( int id, String pass){	
		this.id=id;
		this.pass=pass;		
	}
	
	public String toString(){
		return "\t\t" +id + "\t\t" + "\n";
	}
	
	public String getPass(){
		return pass;
	}
	public void setPass(String x){
		pass = x;
	}
}