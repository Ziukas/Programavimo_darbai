package files;
import java.io.File;
import java.util.*;
import javax.swing.DefaultListModel;


public class UsersMgr {
	DefaultListModel<Users> kolekcija;

	Scanner x;

	public List<Users> user;

	public UsersMgr() {

		user = new ArrayList<Users>();

	}

	public void add() {
		open();
		while (x.hasNext()) {
			int id = x.nextInt();
			String pass = x.next();
			Users b = new Users(id, pass);
			user.add(b);
		}
		close();

	}

	public void open() {
		try {
			x = new Scanner(new File("users.txt"));
		} catch (Exception e) {
			System.out.println("Failas nerastas");
		}
	}

	public void close() {
		x.close();
	}

	public String toString() {
		String total = "\n";
		for (int i = 0; i < user.size(); i++) {
			Users c = user.get(i);
			total = total + i + "\t" + c.toString();
		}
		return total;
	}

	public boolean arYraPass(int id, String pass) {
		for (int i = 0; i < user.size(); i++) {
			if (user.get(i).getId() == id && user.get(i).getPass().equals(pass))
				return true;
		}
		return false;
	}

}
