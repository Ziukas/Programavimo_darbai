package files;

public class Notes {
	
	public String note;
	public int id;
	boolean status;
	

	public Notes(String note, int id, boolean status){
		this.note=note;
		this.id=id;
		this.status = status;
		}
	
	@Override
	public String toString(){
		return note  + " "+ id + " " + status ;
	}
	
	public void setStatus(boolean status) {
		this.status = status;
	}

	public String toString(boolean f){
		return id + " " + status +" "+ note + "\n" ;
	}
	

	public String getNote(){
		return note;	
	}
	
	
	public int getId(){
		return id;	
	}	
	
	public void setNote(String note) {
		this.note = note;
	}


	

}