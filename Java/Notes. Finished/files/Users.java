package files;
public class Users {
	
	private int id;
	private String pass;
	
	
	public Users(int id, String pass){
		this.id = id;
		this.pass = pass;
		
	}
	
	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public void setId(int id) {
		this.id = id;
	}

	//@Override
	public final String toString(){
		return pass + " " + id +"\n";
	}
	
	public int getId(){
		return id;
	}
}