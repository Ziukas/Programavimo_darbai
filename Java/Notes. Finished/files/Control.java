package files;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.util.*;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import main.*;


public class Control extends mainas {
	JFrame listFrame;
	Scanner x;

	private List<Notes> collection;

	DefaultListModel<Notes> kolekcija;

	public Control() {
		collection = new ArrayList<Notes>();
	}

	public void JlistAdmin() {
		JFrame listFrame;
		kolekcija = new DefaultListModel<Notes>();
		for (int i = 0; i < collection.size(); i++) {
			Notes b = collection.get(i);
			kolekcija.addElement(b);
		}

		listFrame = new JFrame();

		JList<Notes> myList = new JList<>(kolekcija);

		JScrollPane scrollPane = new JScrollPane(myList);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		listFrame.add(scrollPane);
		listFrame.add(myList);
		listFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		listFrame.setTitle("Tasks list");
		listFrame.setSize(600, 400);
		listFrame.setLocationRelativeTo(null);
		listFrame.setLocationByPlatform(true);
		listFrame.setVisible(true);
		
		myList.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				JList myList = (JList) evt.getSource();
				if (evt.getClickCount() == 2) {
					int index = myList.locationToIndex(evt.getPoint());
					listFrame.setVisible(false);
					Edit(index);

				}

			}
		});

	}

	public void JlistUser() {
		JFrame listFrame;
		kolekcija = new DefaultListModel<Notes>();
		for (int i = 0; i < collection.size(); i++) {
			if (collection.get(i).getId() == userId) {
				Notes b = collection.get(i);
				kolekcija.addElement(b);
			}
		}

		listFrame = new JFrame();
		JList<Notes> myList = new JList<>(kolekcija);
		JScrollPane scrollPane = new JScrollPane(myList);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		listFrame.add(scrollPane);
		listFrame.add(myList);
		listFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		listFrame.setTitle("Knyg� s�ra�as");
		listFrame.setSize(600, 400);
		listFrame.setLocationRelativeTo(null);
		listFrame.setLocationByPlatform(true);
		listFrame.setVisible(true);
		myList.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				JList myList = (JList) evt.getSource();
				if (evt.getClickCount() == 2) {

					int index = myList.locationToIndex(evt.getPoint());
					collection.get(myList.getSelectedIndex()).setStatus(false);
					for (int i = 0; i < collection.size(); i++) {
						if (collection.get(i) == myList.getSelectedValue()) {
							collection.get(i).setStatus(true);
						}

					}
					listFrame.setVisible(false);
					JlistUser();
				}

			}
		});

	}

	public void addNote(String note, int id) {
		boolean status = false;
		Notes b = new Notes(note, id, status);
		collection.add(b);
	}

//	public String toString() {
//		String total = "\n";
//		for (int i = 0; i < collection.size(); i++) {
//			Notes b = collection.get(i);
//			total = total + i + " " + b.toString();
//		}
//		return total;
//	}

	public void add() {
		open("notes.txt");
		while (x.hasNext()) {
			int id = x.nextInt();
			boolean status = x.nextBoolean();
			String note = x.nextLine();
			// boolean yra = x.nextBoolean();
			Notes b = new Notes(note, id, status);
			collection.add(b);
		}
		close();
	}

	public void open(String n) {
		try {
			x = new Scanner(new File(n));
		} catch (Exception e) {
			System.out.println("Failas nerastas");
		}
	}

	public void safe() {
		File pav = new File("notes.txt");
		try {
			FileWriter fw = new FileWriter(pav);
			Writer output = new BufferedWriter(fw);
			int sz = collection.size();
			for (int i = 0; i < sz; i++) {
				output.write(collection.get(i).toString(true));
			}
			output.close();

		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "kazkas negerai");
		}
	}

	public void close() {
		x.close();

	}

	public void taisymas(int x) {
		JFrame frame = new JFrame("Notes");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel contentPane = new JPanel();
		contentPane.setOpaque(true);
		contentPane.setBackground(Color.WHITE);
		contentPane.setLayout(null);

		JLabel label = new JLabel("Edit a note", JLabel.CENTER);
		label.setSize(300, 30);
		label.setLocation(145, 5);
		label.setFont(new Font("Comic Sans", Font.ITALIC, 20));

		JButton add = new JButton("Add");
		add.setSize(100, 30);
		add.setLocation(250, 180);

		JTextField note = new JTextField("Task");
		note.setSize(300, 30);
		note.setLocation(145, 45);



		contentPane.add(label);
		contentPane.add(add);
		contentPane.add(note);

		frame.setContentPane(contentPane);
		frame.setSize(600, 400);
		frame.setLocationByPlatform(true);
		frame.setVisible(true);

		// center the jframe on screen
		frame.setLocationRelativeTo(null);

		frame.setVisible(true);

		add.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent ae) {
				String text = note.getText();
				text = text + " ";
				collection.get(x).setNote(text);
				frame.setVisible(false);
				JlistAdmin();
			}
		});

	}

	public void Edit(int index) {
		JFrame frame = new JFrame("Notes");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel contentPane = new JPanel();
		contentPane.setOpaque(true);
		contentPane.setBackground(Color.WHITE);
		contentPane.setLayout(null);

		JLabel label = new JLabel("Choose an option:", JLabel.CENTER);
		label.setSize(300, 30);
		label.setLocation(145, 5);
		label.setFont(new Font("Courier New", Font.ITALIC, 20));

		JButton button = new JButton("Edit");
		button.setSize(100, 30);
		button.setLocation(100, 300);
		JButton button2 = new JButton("Remove");
		button2.setSize(100, 30);
		button2.setLocation(400, 300);

		contentPane.add(label);
		contentPane.add(button);
		contentPane.add(button2);

		frame.setContentPane(contentPane);
		frame.setSize(600, 400);
		frame.setLocationByPlatform(true);
		frame.setVisible(true);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);

		button.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent ae) {
				taisymas(index);
				frame.setVisible(false);
			}
		});

		button2.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent ae) {
				collection.remove(index);
				frame.setVisible(false);
				JlistAdmin();
			}
		});

	}

}