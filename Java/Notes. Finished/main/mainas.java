package main;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import files.*;

public class mainas {

	public static int userId;
	static Control ctrl = new Control();
	static UsersMgr user = new UsersMgr();
	static AdminsMgr admin = new AdminsMgr();

	public static void main(String[] args) {
		user.add();
		ctrl.add();
		admin.addAdmins();
		Launch();
	}

	public static void Launch() {
		JFrame frame = new JFrame("Notes");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(600, 600);
		JPanel contentPane = new JPanel();
		contentPane.setOpaque(true);
		contentPane.setBackground(Color.WHITE);
		contentPane.setLayout(null);

		JLabel label = new JLabel("Sign In", JLabel.CENTER);
		label.setSize(300, 30);
		label.setLocation(145, 5);
		label.setFont(new Font("Courier New", Font.ITALIC, 20));

		JButton button = new JButton("Admin");
		button.setSize(100, 30);
		button.setLocation(100, 130);

		JButton button2 = new JButton("User");
		button2.setSize(100, 30);
		button2.setLocation(400, 130);

		contentPane.add(label);
		contentPane.add(button);
		contentPane.add(button2);

		frame.setContentPane(contentPane);
		frame.setSize(600, 400);
		frame.setLocationByPlatform(true);
		frame.setVisible(true);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);

		button.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent ae) {
				Admin();
				frame.setVisible(false);

			}
		});

		button2.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent ae) {
				User();
				frame.setVisible(false);

			}
		});

	}

	public static void Admin() {
		JFrame frame = new JFrame("Notes");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel contentPane = new JPanel();
		contentPane.setOpaque(true);
		contentPane.setBackground(Color.WHITE);
		contentPane.setLayout(null);

		JLabel label = new JLabel("Enter ID and password", JLabel.CENTER);
		label.setSize(300, 30);
		label.setLocation(145, 5);
		label.setFont(new Font("Courier New", Font.ITALIC, 20));

		JButton button = new JButton("Login");
		button.setSize(100, 30);
		button.setLocation(250, 130);

		JLabel label2 = new JLabel("ID:");
		label2.setSize(300, 30);
		label2.setLocation(145, 25);
		label2.setFont(new Font("Courier New", Font.ITALIC, 12));

		JTextField field = new JTextField("0000");
		field.setSize(300, 30);
		field.setLocation(145, 45);

		JLabel label3 = new JLabel("Password:");
		label3.setSize(300, 30);
		label3.setLocation(145, 68);
		label3.setFont(new Font("Courier New", Font.ITALIC, 12));

		JTextField field2 = new JTextField("0000");
		field2.setSize(300, 30);
		field2.setLocation(145, 90);

		contentPane.add(label);
		contentPane.add(button);
		contentPane.add(field);
		contentPane.add(field2);
		contentPane.add(label2);
		contentPane.add(label3);
		frame.setContentPane(contentPane);
		frame.setSize(600, 400);
		frame.setLocationByPlatform(true);
		frame.setVisible(true);
		frame.setLocationRelativeTo(null);

		button.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent ae) {

				String text = field.getText();

				try {
					int id = Integer.valueOf(text);
					String text2 = field2.getText();
					if (admin.arYraPass(id, text2)) {
						frame.setVisible(false);
						javax.swing.SwingUtilities.invokeLater(new Runnable() {
							public void run() {
								AdminGui();
							}
						});

					} else {
						label.setSize(400, 30);
						label.setFont(new Font("Courier New", Font.ITALIC, 12));
						label.setText("Wrong ID or password");
					}
				} catch (Exception c) {
					System.out.println("Blogai ivestas id, bandykite is naujo");
					System.out.println(c.getMessage());
				}

			}
		});

	}

	public static void User() {
		JFrame frame = new JFrame("Notes");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel contentPane = new JPanel();
		contentPane.setOpaque(true);
		contentPane.setBackground(Color.WHITE);
		contentPane.setLayout(null);

		JLabel label = new JLabel("Enter ID and password", JLabel.CENTER);
		label.setSize(300, 30);
		label.setLocation(145, 5);
		label.setFont(new Font("Courier New", Font.ITALIC, 20));

		JButton button = new JButton("Login");
		button.setSize(100, 30);
		button.setLocation(250, 130);

		JLabel label2 = new JLabel("ID:");
		label2.setSize(300, 30);
		label2.setLocation(145, 25);
		label2.setFont(new Font("Courier New", Font.ITALIC, 12));

		JTextField field = new JTextField("0000");
		field.setSize(300, 30);
		field.setLocation(145, 45);

		JLabel label3 = new JLabel("Password:");
		label3.setSize(300, 30);
		label3.setLocation(145, 68);
		label3.setFont(new Font("Courier New", Font.ITALIC, 12));

		JTextField field2 = new JTextField("0000");
		field2.setSize(300, 30);
		field2.setLocation(145, 90);

		contentPane.add(label);
		contentPane.add(button);
		contentPane.add(field);
		contentPane.add(field2);
		contentPane.add(label2);
		contentPane.add(label3);
		frame.setContentPane(contentPane);
		frame.setSize(600, 400);
		frame.setLocationByPlatform(true);
		frame.setVisible(true);

		frame.setLocationRelativeTo(null);

		frame.setVisible(true);

		button.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent ae) {

				String text = field.getText();
				try {
					int id = Integer.valueOf(text);
					String text2 = field2.getText();
					if (user.arYraPass(id, text2)) {
						frame.setVisible(false);
						javax.swing.SwingUtilities.invokeLater(new Runnable() {
							@Override
							public void run() {
								userId = id;
								UserGui();
							}
						});

					} else {
						frame.setVisible(false);
						User();
						label.setSize(400, 30);
						label.setFont(new Font("Courier New", Font.ITALIC, 12));
						label.setText("Wrong ID or password");
					}
				} catch (Exception c) {
					System.out.println("Blogai ivestas id, bandykite is naujo");
					System.out.println(c.getMessage());
				}

			}
		});

	}

	public static void AdminGui() {

		JFrame frame = new JFrame("Notes");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel contentPane = new JPanel();
		contentPane.setOpaque(true);
		contentPane.setBackground(Color.WHITE);
		contentPane.setLayout(null);

		JLabel label = new JLabel("MENU", JLabel.CENTER);
		label.setSize(300, 30);
		label.setLocation(145, 5);
		label.setFont(new Font("Courier New", Font.ITALIC, 20));

		JButton prideti = new JButton("Add");
		prideti.setSize(100, 30);
		prideti.setLocation(100, 300);
		JButton spausd = new JButton("Manage");
		spausd.setSize(100, 30);
		spausd.setLocation(400, 300);

		JButton save = new JButton("Save");
		save.setSize(100, 30);
		save.setLocation(250, 300);

		contentPane.add(label);
		contentPane.add(prideti);
		contentPane.add(spausd);
		contentPane.add(save);
		frame.setContentPane(contentPane);
		frame.setSize(600, 600);
		frame.setLocationByPlatform(true);
		frame.setVisible(true);

		frame.setLocationRelativeTo(null);

		frame.setVisible(true);

		prideti.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent ae) {

				frame.setVisible(false);
				addTask();

			}
		});

		spausd.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent ae) {

				ctrl.JlistAdmin();
				frame.setVisible(true);

			}
		});
		save.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent ae) {

				ctrl.safe();
				frame.setVisible(true);

			}
		});
	}

	public static void UserGui() {
		JFrame frame = new JFrame("Notes");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel contentPane = new JPanel();
		contentPane.setOpaque(true);
		contentPane.setBackground(Color.WHITE);
		contentPane.setLayout(null);

		JLabel label = new JLabel("MENU", JLabel.CENTER);
		label.setSize(300, 30);
		label.setLocation(145, 5);
		label.setFont(new Font("Courier New", Font.ITALIC, 20));

		JButton prideti = new JButton("Review");
		prideti.setSize(100, 30);
		prideti.setLocation(100, 300);
		JButton spausd = new JButton("Safe");
		spausd.setSize(100, 30);
		spausd.setLocation(400, 300);

		contentPane.add(label);
		contentPane.add(prideti);
		contentPane.add(spausd);

		frame.setContentPane(contentPane);
		frame.setSize(600, 600);
		frame.setLocationByPlatform(true);
		frame.setVisible(true);

		frame.setLocationRelativeTo(null);

		frame.setVisible(true);

		prideti.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent ae) {
				ctrl.JlistUser();
			}
		});

		spausd.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent ae) {
				ctrl.safe();
				frame.setVisible(true);

			}
		});

	}

	public static void addTask() {
		JFrame frame = new JFrame("Notes");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel contentPane = new JPanel();
		contentPane.setOpaque(true);
		contentPane.setBackground(Color.WHITE);
		contentPane.setLayout(null);

		JLabel label = new JLabel("Add Notes", JLabel.CENTER);
		label.setSize(300, 30);
		label.setLocation(145, 5);
		label.setFont(new Font("Comic Sans", Font.ITALIC, 20));

		JButton saugoti = new JButton("Add");
		saugoti.setSize(100, 30);
		saugoti.setLocation(250, 180);

		JTextField note = new JTextField("Task");
		note.setSize(300, 30);
		note.setLocation(145, 45);

		JTextField id = new JTextField("User ID");
		id.setSize(300, 30);
		id.setLocation(145, 135);

		contentPane.add(label);
		contentPane.add(saugoti);
		contentPane.add(note);
		contentPane.add(id);

		frame.setContentPane(contentPane);
		frame.setSize(600, 400);
		frame.setLocationByPlatform(true);
		frame.setVisible(true);


		frame.setLocationRelativeTo(null);

		frame.setVisible(true);

		saugoti.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent ae) {

				String text = note.getText();
				text = " " + text + " ";
				String text2 = id.getText();
				try {
					int id = Integer.valueOf(text2);
					ctrl.addNote(text, id);
				} catch (Exception c) {
					System.out.println("Blogai ivestas skaicius, knyga neprideta");
					System.out.println(c.getMessage());
				}
				frame.setVisible(false);
				AdminGui();
			}
		});

	}

}
